<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\EmployeePermission;
use App\Models\Permission;
use App\Models\User;
use App\Traits\PermissionsTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{
    use PermissionsTrait;

    public function add()
    {
        $permission = Permission::where('permission_name', 'إدارة الموظفين')->first();
        if ($this->checkPermission($permission->id)) {

            $permissions = Permission::get();
            $employees = User::with('permissions')->get();

            return view('employee', compact('permissions', 'employees'));
        } else
            return view('noPermission');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'userName' => 'required|string|between:2,100|unique:users',
            'email' => 'required|string|max:250|unique:users',
            'password' => 'required|min:8',


        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $user = new User;
        $user->name = $request->name;
        $user->userName = $request->userName;
        $user->password = Hash::make($request->password);
        $user->email = $request->email;
        $user->save();

        return redirect()->back()->with('success', 'تم إضافة الموظف بنجاح');
    }

    public function delete($id)
    {
        $employee = User::where('id', $id)->first();
        if ($employee) {

            $employee->delete();
            return redirect()->back()->with('success', 'تم حذف الموظف بنجاح');
        } else {
            return redirect()->back()->with('fail', 'حدث خطأ ما');
        }
    }

    public function updatePermission(Request $request, $id)
    {
       $userPer=EmployeePermission::where('user_id',$id)->get();
       if($userPer)
       {
           foreach ($userPer as $per)
           {
               $per->delete();
           }
       }
        if($request->permissions)
        {
            foreach ($request->permissions as $per)
            {
                $newPer=new EmployeePermission;
                $newPer->user_id=$id;
                $newPer->permission_id=$per;
                $newPer->save();
            }
        }
        return redirect()->back()->with('success', 'تم تعديل الصلاحيات بنجاح');
    }
    public function update(Request $request)
    {
        $user =  User::where('id',$request->id)->first();

        if($user->userName==$request->userName && $user->email==$request->email)
        {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|between:2,100',
            ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            $user->name = $request->name;
            $user->email = $request->email;
        }
      else  if($user->email==$request->email)
        {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|between:2,100',
                'userName' => 'required|string|between:2,100|unique:users',

            ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            $user->name = $request->name;
            $user->userName = $request->userName;
        }
      else if($user->userName==$request->userName  )
        {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|between:2,100',
                'email' => 'required|string|max:250|unique:users',
            ]);
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
            $user->name = $request->name;
        }

        $user->save();
        return redirect()->back()->with('success', 'تم تعديل الموظف بنجاح');
    }

}
