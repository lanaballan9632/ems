<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Surename;
use App\Models\Surename2;
use App\Models\User;
use Illuminate\Http\Request;

class SurenameController extends Controller
{
    public function add1()
    {
            $sureName = Surename::get();
            return view('sureName1', compact('sureName'));
    }
    public function add2()
    {
        $sureName = Surename2::get();
        return view('sureName2', compact('sureName'));
    }
    public function store1(Request $request)
    {
        $sureName=new Surename;
        $sureName->name=$request->name;
        $sureName->lang=$request->selectedLanguage;
        $sureName->save();
        return redirect()->back()->with('success', 'تم إضافة اللقب بنجاح');
    }
    public function store2(Request $request)
    {
        $sureName=new Surename2;
        $sureName->name=$request->name;
        $sureName->lang=$request->selectedLanguage;
        $sureName->save();
        return redirect()->back()->with('success', 'تم إضافة اللقب بنجاح');
    }
    public function delete1($id)
    {
        $sureName = Surename::where('id', $id)->first();
        if ($sureName) {

            $sureName->delete();
            return redirect()->back()->with('success', 'تم حذف اللقب بنجاح');
        } else {
            return redirect()->back()->with('fail', 'حدث خطأ ما');
        }
    }
    public function delete2($id)
{
    $sureName = Surename2::where('id', $id)->first();
    if ($sureName) {

        $sureName->delete();
        return redirect()->back()->with('success', 'تم حذف اللقب بنجاح');
    } else {
        return redirect()->back()->with('fail', 'حدث خطأ ما');
    }
}
    public function update1(Request $request)
    {
        $sureName=Surename::where('id',$request->sureId)->first();
        if($sureName)
        {
            $sureName->name=$request->name;
            $sureName->lang=$request->selectedLanguage;
            $sureName->save();
            return redirect()->back()->with('success', 'تم تعديل اللقب بنجاح');
        }
       else{
           return redirect()->back()->with('fail', 'حدث خطأ ما');
       }

    }
    public function update2(Request $request)
    {
        $sureName=Surename2::where('id',$request->sureId)->first();
        if($sureName)
        {
            $sureName->name=$request->name;
            $sureName->lang=$request->selectedLanguage;
            $sureName->save();
            return redirect()->back()->with('success', 'تم تعديل اللقب بنجاح');
        }
        else{
            return redirect()->back()->with('fail', 'حدث خطأ ما');
        }

    }
}
