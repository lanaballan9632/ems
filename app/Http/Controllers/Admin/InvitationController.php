<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\GeneralInvitation;
use App\Models\Group;
use App\Models\Invitation;
use App\Models\Permission;
use App\Models\Seat;
use App\Models\SendInvitation;
use App\Models\Surename;
use App\Models\Surename2;
use App\Models\User;
use App\Traits\PermissionsTrait;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Mail\InvitationMail;
use Illuminate\Support\Facades\Mail;
use App\Mail\InvitationMailEn;

class InvitationController extends Controller
{
    use PermissionsTrait;

    public function add()
    {

        $permission = Permission::where('permission_name', 'إرسال الدعوات')->first();
        if ($this->checkPermission($permission->id)) {

            $invitations = SendInvitation::get();
            $sureName = Surename::get();
            $sureName2 = Surename2::get();
            $groups = Group::get();
            return view('invitation', compact('invitations', 'sureName','sureName2','groups'));
        } else
            return view('noPermission');
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|max:250|unique:send_invitations|unique:general_invitations',
            'name' => 'required|string|max:250|unique:send_invitations|unique:general_invitations',

        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $invitation=new SendInvitation;
        $invitation->surename=$request->surename;
        $invitation->surename2=$request->surename2;
        $invitation->group=$request->group;
        $invitation->name=$request->name;
        $invitation->email=$request->email;
        $invitation->extra_email=$request->extra_email;
        $invitation->destination=$request->destination;
        $invitation->number=$request->number;
        $invitation->position=$request->position;
        $invitation->lang=$request->selectedLanguage;
        if($request->send_email=='true')
        $invitation->send_email=true;
        else
            $invitation->send_email=false;
        if($request->send_message=='true')
            $invitation->send_message=true;
        else
            $invitation->send_message=false;

        $invitation->save();
if($request->send_email=='true')
{
    $surename=Surename::where('id',$request->surename)->first();
    $surename2=Surename2::where('id',$request->surename2)->first();
    try{
        $mailData=['surename'=>$surename->name,'surename2'=>$surename2->name
            ,'name'=>$request->name,'invitation_id'=>$invitation->id];
if($request->selectedLanguage=='عربي')
        Mail::to($request->email)->send(new InvitationMail($mailData));
else
    Mail::to($request->email)->send(new InvitationMailEn($mailData));
    }
    catch (Exception $ex)
    {
        return redirect()->back()->with('fail', 'حدث خطأ ما');
    }
}
        return redirect()->back()->with('success', 'تم إرسال الدعوة بنجاح');
    }
    public function delete($id){
        $invitation=SendInvitation::where('id',$id)->first();
        if($invitation){
            $invitation->delete();
            return redirect()->back()->with('success', 'تم حذف الدعوة بنجاح');
        }
        else
        {
            return redirect()->back()->with('fail', 'حدث خطأ ما');
        }
    }
    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|max:250|unique:general_invitations|unique:send_invitations',
            'name' => 'required|string|max:250|unique:send_invitations|unique:general_invitations',

        ]);
        $invitation=SendInvitation::where('id',$request->id)->first();
        if(!$invitation)
        {
            return redirect()->back()->with('fail', 'الدعوة غير موجودة');
        }
        if($invitation->email!=$request->email)
        {
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
        }
        $invitation->surename=$request->surename;
        $invitation->surename2=$request->surename2;
        $invitation->group=$request->group;
        $invitation->name=$request->name;
        $invitation->email=$request->email;
        $invitation->extra_email=$request->extra_email;
        $invitation->destination=$request->destination;
        $invitation->number=$request->number;
        $invitation->position=$request->position;
        $invitation->lang=$request->selectedLanguage;
        if($request->send_email=='true')
            $invitation->send_email=true;
        else if($request->send_email=='false')
            $invitation->send_email=false;
        else
            $invitation->send_email=$request->send_email;
        if($request->send_message=='true')
            $invitation->send_message=true;
        else if($request->send_message=='false')
            $invitation->send_message=false;
        else
            $invitation->send_message=$request->send_message;
        if($request->confirm=='true')
            $invitation->confirm=true;
        else if($request->confirm=='false')
            $invitation->confirm=false;
        else
            $invitation->confirm=$request->confirm;
        $invitation->save();
        if($request->confirm=='true'||$request->confirm)
        {
            $confirmed=new Invitation;
            $confirmed->surename=$invitation->surename;
            $confirmed->surename2=$invitation->surename2;
            $confirmed->original_id=$invitation->id;
            $confirmed->group=$invitation->group;
            $confirmed->email=$invitation->email;
            $confirmed->name=$invitation->name;
            $confirmed->invitation_type='دعوة';
            $confirmed->destination=$invitation->destination;
            $confirmed->position=$invitation->position;
            $confirmed->number=$invitation->number;
            $confirmed->save();
        }

        if($request->send_email=='true'||$request->send_email==1)
        {
            $surename=Surename::where('id',$request->surename)->first();
            $surename2=Surename2::where('id',$request->surename2)->first();
            try{
                $mailData=['surename'=>$surename->name,'surename2'=>$surename2->name
                    ,'name'=>$request->name,'invitation_id'=>$invitation->id];
                if($request->selectedLanguage=='عربي')
                    Mail::to($request->email)->send(new InvitationMail($mailData));
                else
                    Mail::to($request->email)->send(new InvitationMailEn($mailData));
            }
            catch (Exception $ex)
            {
                return redirect()->back()->with('fail', 'حدث خطأ ما');
            }
        }
        return redirect()->back()->with('success', 'تم تعديل الدعوة بنجاح');
    }
    public function confirmMail($id){
        $invitation=SendInvitation::where('id',$id)->first();
        return view('confirmMail',compact('invitation'));
    }
    public function submitInvitation($id){
        $invitation=SendInvitation::where('id',$id)->first();
        $invitation->confirm=true;
        $invitation->save();
        $confirmed=new Invitation;
        $confirmed->surename=$invitation->surename;
        $confirmed->surename2=$invitation->surename2;
        $confirmed->original_id=$invitation->id;
        $confirmed->group=$invitation->group;
        $confirmed->email=$invitation->email;
        $confirmed->name=$invitation->name;
        $confirmed->invitation_type='دعوة';
        $confirmed->destination=$invitation->destination;
        $confirmed->position=$invitation->position;
        $confirmed->number=$invitation->number;
        $confirmed->save();
        return view('successSubmit');
    }
    public function all(){
        $permission = Permission::where('permission_name', 'الدعوات المقبولة يوم الحفل')->first();
        if ($this->checkPermission($permission->id)) {
            $invitations = Invitation::get();
            $sureName = Surename::get();
            $sureName2 = Surename2::get();
            $groups = Group::get();
            $seats = Seat::where('status','فارغ')->get();
            return view('allInvitations', compact('invitations', 'sureName', 'sureName2', 'groups', 'seats'));
        }
        else
            return view('noPermission');
    }
    public function storeConfirmedInvitation(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|max:250|unique:general_invitations|unique:send_invitations',
            'name' => 'required|string|max:250|unique:send_invitations|unique:general_invitations',

        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $confirmed=new Invitation;
        $confirmed->surename=$request->surename;
        $confirmed->surename2=$request->surename2;
        $confirmed->original_id=null;
        $confirmed->group=$request->group;
        $confirmed->email=$request->email;
        $confirmed->name=$request->name;
        $confirmed->invitation_type='تسجيل';
        $confirmed->destination=$request->destination;
        $confirmed->position=$request->position;
        $confirmed->number=$request->number;
        if($request->confirm=='true')
        $confirmed->attend=true;
        else if($request->confirm=='false')
            $confirmed->attend=false;
        else
            $confirmed->attend=$request->confirm;
        $confirmed->save();
        return redirect()->back()->with('success', 'تم إضافة الدعوة بنجاح');
    }
        public function updateConfirmedInvitation(Request $request){
        $validator = Validator::make($request->all(), [
        'email' => 'required|string|max:250|unique:send_invitations|unique:general_invitations',
            'name' => 'required|string|max:250|unique:send_invitations|unique:general_invitations',

        ]);
        $invitation=Invitation::where('id',$request->id)->first();
        if(!$invitation)
        {
        return redirect()->back()->with('fail', 'الدعوة غير موجودة');
        }
        if($invitation->email!=$request->email)
        {
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
        }
            $invitation->surename=$request->surename;
            $invitation->surename2=$request->surename2;
            $invitation->group=$request->group;
            $invitation->email=$request->email;
            $invitation->name=$request->name;
            $invitation->destination=$request->destination;
            $invitation->position=$request->position;
            $invitation->number=$request->number;
            if($request->confirm=='true')
                $invitation->attend=true;
            else if($request->confirm=='false')
                $invitation->attend=false;
            else
                $invitation->attend=$request->confirm;
            $invitation->save();

            if($invitation->invitation_type=='تسجيل')
            $original=GeneralInvitation::where('id',$invitation->original_id)->first();
            else
                $original=SendInvitation::where('id',$invitation->original_id)->first();
            $original->surename=$request->surename;
            $original->surename2=$request->surename2;
            $original->group=$request->group;
            $original->email=$request->email;
            $original->name=$request->name;
            $original->destination=$request->destination;
            $original->position=$request->position;
            $original->number=$request->number;
$original->save();
            return redirect()->back()->with('success', 'تم تعديل الدعوة بنجاح');
}
public function deleteConfirmedInvitation($id){
    $invitation=Invitation::where('id',$id)->first();
    if($invitation){
        if($invitation->seat_id!=null)
        {
            $seat=Seat::where('seat_id',$invitation->seat_id)->first();
            $seat->status='فارغ';
            $seat->person_name = null;
            $seat->save();
        }
        $invitation->delete();
        return redirect()->back()->with('success', 'تم حذف الدعوة بنجاح');
    }
    else
    {
        return redirect()->back()->with('fail', 'حدث خطأ ما');
    }
}
public function setSeat(Request $request){
    $permission = Permission::where('permission_name', 'تعيين الكراسي')->first();
    if ($this->checkPermission($permission->id)) {
        $invitation = Invitation::where('id', $request->invitationId)->first();
        if($invitation->seat_id!=null)
        {  $oldSeat=Seat::where('seat_id', $invitation->seat_id)->first();
            $oldSeat->status='فارغ';
            $oldSeat->person_name = null;
            $oldSeat->save();
        }
        $seat = Seat::where('seat_id', $request->seatId)->first();
        $seat->person_name = $invitation->name;
        $seat->status='محجوز';
        $seat->save();
        $invitation->seat_id = $seat->seat_id;
        $invitation->save();
        return redirect()->back()->with('success', 'تم تعديل المقعد بنجاح');
    }
    else
        return view('noPermission');

}
public function allSeats(){
        $seats=Seat::get();
    $names=Invitation::where('seat_id',null)->select('name')->get();
        return view('allSeats',compact('seats','names'));
}
    public function emptySeats(){
        $seats = Seat::where('status','فارغ')->get();
        $names=Invitation::where('seat_id',null)->select('name')->get();
        return view('emptySeats',compact('seats','names'));
    }
    public function updateSeat(Request $request){
        $seat=Seat::where('id',$request->id)->first();
        if($seat->person_name!=null){
            $oldinvitation=Invitation::where('seat_id', $seat->seat_id)->first();
            $oldinvitation->seat_id=null;
            $oldinvitation->save();
        }
        $seat->person_name=$request->person;
        $seat->status='محجوز';
        $seat->save();
        $invitation=Invitation::where('name',$request->person)->first();
        $invitation->seat_id = $seat->seat_id;
        $invitation->save();
        return redirect()->back()->with('success', 'تم تعديل المقعد بنجاح');

    }

}
