<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\GeneralInvitationMail;
use App\Mail\GeneralInvitationMailAccept;
use App\Mail\GeneralInvitationMailReject;
use App\Mail\InvitationMail;
use App\Mail\InvitationMailEn;
use App\Models\GeneralInvitation;
use App\Models\Group;
use App\Models\Invitation;
use App\Models\Permission;
use App\Models\SendInvitation;
use App\Models\Surename;
use App\Models\Surename2;
use App\Traits\PermissionsTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Exception;
class GeneralInvitationController extends Controller
{
    use PermissionsTrait;

    public function add()
    {
        $permission = Permission::where('permission_name', 'الدعوات العامة')->first();
        if ($this->checkPermission($permission->id)) {

            $invitations = GeneralInvitation::get();
            $sureName = Surename::get();
            $sureName2 = Surename2::get();
            $groups = Group::get();
            return view('generalInvitation', compact('invitations', 'sureName','sureName2','groups'));
        } else
            return view('noPermission');
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|max:250|unique:send_invitations|unique:general_invitations',
            'name' => 'required|string|max:250|unique:send_invitations|unique:general_invitations',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $invitation=new GeneralInvitation;
        $invitation->surename=$request->surename;
        $invitation->surename2=$request->surename2;
        $invitation->group=$request->group;
        $invitation->name=$request->name;
        $invitation->email=$request->email;
        $invitation->destination=$request->destination;
        $invitation->number=$request->number;
        $invitation->position=$request->position;
        $invitation->req_status=$request->req_status;
        $invitation->register_type='داخلي';
        if($request->send_email=='true')
            $invitation->send_email=true;
        else
            $invitation->send_email=false;
        if($request->confirm=='true')
            $invitation->attend=true;
        else
            $invitation->attend=false;
        $invitation->save();

if($request->req_status=='تم التأكيد')
{
    $confirmed=new Invitation;
    $confirmed->surename=$invitation->surename;
    $confirmed->surename2=$invitation->surename2;
    $confirmed->original_id=$invitation->id;
    $confirmed->group=$invitation->group;
    $confirmed->email=$invitation->email;
    $confirmed->name=$invitation->name;
    $confirmed->invitation_type='تسجيل';
    $confirmed->destination=$invitation->destination;
    $confirmed->position=$invitation->position;
    $confirmed->number=$invitation->number;
    $confirmed->save();
}

        if($request->send_email=='true' && $request->req_status=='تم التأكيد')
        {
            $surename=Surename::where('id',$request->surename)->first();
            $surename2=Surename2::where('id',$request->surename2)->first();
            try{
                $mailData=['surename'=>$surename->name,'surename2'=>$surename2->name
                    ,'name'=>$request->name];
                    Mail::to($request->email)->send(new GeneralInvitationMailAccept($mailData));

            }
            catch (Exception $ex)
            {
                return redirect()->back()->with('fail', 'حدث خطأ ما');
            }
        }
        return redirect()->back()->with('success', 'تم إرسال الدعوة بنجاح');
    }
    public function delete($id){
        $invitation=GeneralInvitation::where('id',$id)->first();
        if($invitation){
            $invitation->delete();
            return redirect()->back()->with('success', 'تم حذف الدعوة بنجاح');
        }
        else
        {
            return redirect()->back()->with('fail', 'حدث خطأ ما');
        }
    }
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|max:250|unique:send_invitations|unique:general_invitations',
            'name' => 'required|string|max:250|unique:send_invitations|unique:general_invitations',
        ]);
        $invitation=GeneralInvitation::where('id',$request->id)->first();
        if(!$invitation)
        {
            return redirect()->back()->with('fail', 'الدعوة غير موجودة');
        }
        if($invitation->email!=$request->email || $invitation->name!=$request->name)
        {
            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
            }
        }

        $invitation->surename=$request->surename;
        $invitation->surename2=$request->surename2;
        $invitation->group=$request->group;
        $invitation->name=$request->name;
        $invitation->email=$request->email;
        $invitation->destination=$request->destination;
        $invitation->number=$request->number;
        $invitation->position=$request->position;
        $invitation->req_status=$request->req_status;
        if($request->send_email=='true')
            $invitation->send_email=true;
        else if($request->send_email=='false')
            $invitation->send_email=false;
        else
            $invitation->send_email=$request->send_email;
        if($request->confirm=='true')
            $invitation->attend=true;
        else if($request->confirm=='false')
            $invitation->attend=false;
        else
            $invitation->attend=$request->confirm;
        $invitation->save();

        if($request->req_status=='تم التأكيد')
        {
            $oldconfirmed=Invitation::where('email',$invitation->email)->first();
            if(!$oldconfirmed)
            {
                $confirmed=new Invitation;
                $confirmed->surename=$invitation->surename;
                $confirmed->surename2=$invitation->surename2;
                $confirmed->original_id=$invitation->id;
                $confirmed->group=$invitation->group;
                $confirmed->email=$invitation->email;
                $confirmed->name=$invitation->name;
                $confirmed->invitation_type='تسجيل';
                $confirmed->destination=$invitation->destination;
                $confirmed->position=$invitation->position;
                $confirmed->number=$invitation->number;
                $confirmed->save();
            }

        }
        if(($request->send_email=='true'||$request->send_email) && $request->req_status=='تم التأكيد')
        {
            $surename=Surename::where('id',$request->surename)->first();
            $surename2=Surename2::where('id',$request->surename2)->first();
            try{
                if($surename)
                {
                    $mailData=['surename'=>$surename->name,'surename2'=>$surename2->name
                        ,'name'=>$request->name];
                    Mail::to($request->email)->send(new GeneralInvitationMailAccept($mailData));
                }
else{
    $mailData=['surename2'=>$surename2->name
        ,'name'=>$request->name];
    Mail::to($request->email)->send(new GeneralInvitationMailAccept($mailData));
}
            }
            catch (Exception $ex)
            {
                return redirect()->back()->with('fail', 'حدث خطأ ما');
            }
        }
        if(($request->send_email=='true'||$request->send_email) && $request->req_status=='تم الاعتذار')
        {
            $surename=Surename::where('id',$request->surename)->first();
            $surename2=Surename2::where('id',$request->surename2)->first();
            try{
                if($surename)
                {
                    $mailData=['surename'=>$surename->name,'surename2'=>$surename2->name
                        ,'name'=>$request->name];

                    Mail::to($request->email)->send(new GeneralInvitationMailReject($mailData));
                }
            else{
                $mailData=['surename2'=>$surename2->name
                    ,'name'=>$request->name];

                Mail::to($request->email)->send(new GeneralInvitationMailReject($mailData));
            }
            }
            catch (Exception $ex)
            {
                return redirect()->back()->with('fail', 'حدث خطأ ما');
            }
        }
        return redirect()->back()->with('success', 'تم تعديل الدعوة بنجاح');
    }
    public function addReq(){
        $sureName= Surename2::get();
        return view('invitationReq',compact('sureName'));
    }
public function storeReq(Request $request)
{
    $validator = Validator::make($request->all(), [
        'email' => 'required|string|max:250|unique:general_invitations|unique:send_invitations',
        'name' => 'required|string|max:250|unique:send_invitations|unique:general_invitations',

    ]);
    if ($validator->fails()) {
        return redirect()->back()
            ->withErrors($validator)
            ->withInput();
    }

    $invitation=new GeneralInvitation;
    $invitation->surename2=$request->surename;
    $invitation->name=$request->name;
    $invitation->email=$request->email;
    $invitation->destination=$request->destination;
    $invitation->number=$request->number;
    $invitation->position=$request->position;
    $invitation->req_status='قيد الدراسة';
    $invitation->register_type='خارجي';
        $invitation->send_email=true;
        $invitation->attend=false;
    $invitation->save();
        $surename=Surename2::where('id',$request->surename)->first();
        try{
            $mailData=['surename'=>$surename->name
                ,'name'=>$request->name];
            Mail::to($request->email)->send(new GeneralInvitationMail($mailData));
        }
        catch (Exception $ex)
        {
            return redirect()->back()->with('fail', 'حدث خطأ ما');
        }

    return view('successInvitationReq');
}
}
