<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Group;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    public function add()
    {
        $groups = Group::get();
        return view('group', compact('groups'));
    }
    public function store(Request $request)
    {
        $group=new Group;
        $group->name=$request->name;
        $group->save();
        return redirect()->back()->with('success', 'تم إضافة الفئة بنجاح');
    }
    public function delete($id)
    {
        $group = Group::where('id', $id)->first();
        if ($group) {

            $group->delete();
            return redirect()->back()->with('success', 'تم حذف الفئة بنجاح');
        } else {
            return redirect()->back()->with('fail', 'حدث خطأ ما');
        }
    }
    public function update(Request $request)
    {
        $group=Group::where('id',$request->groupId)->first();
        if($group)
        {
            $group->name=$request->name;
            $group->save();
            return redirect()->back()->with('success', 'تم تعديل الفئة بنجاح');
        }
        else{
            return redirect()->back()->with('fail', 'حدث خطأ ما');
        }

    }
}
