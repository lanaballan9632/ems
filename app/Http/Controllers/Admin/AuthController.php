<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
//use App\Mail\ResetPassword;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function home()
    {
        return view('master');
    }
    public function check(Request $request)
    {
        $request->validate([
            'userName' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:8'],
        ]);
        $user = User::where('userName', $request->userName)->first();
        if ($user != null) {
            if(Hash::check($request->password,$user->password )) {
                $credentials = $request->only('userName', 'password');
                if($request->remember){
                    $remember = $request->has('remember');
                    Auth::guard('web')->attempt($credentials,$remember);
                }
              else
                  Auth::guard('web')->attempt($credentials);
                return redirect()->route('home');
            }
            return redirect()->route('login')->with('fail', 'البيانات غير صحيحة');
        }
        return redirect()->route('login')->with('fail', 'البيانات غير صحيحة');
    }

    public function adminLogout()
    {

        Auth::guard('web')->logout();
        return redirect()->route('login');
    }

//
//
//
//    public function forgotPass(Request $request)
//    {
//        $validator = Validator::make($request->all(), [
//            'email' => ['required', 'string', 'email', 'max:255'],
//        ]);
//        if ($validator->fails()) {
//            return redirect('password.forgot')
//                ->withErrors($validator)
//                ->withInput();
//        }
//
//        $verify = User::where('email', $request->all()['email'])->exists();
//        $information = User::where('email', $request->all()['email'])->first();
//
//        if ($verify) {
//            $verify2 = LoginInformation::where('user_id', $information->id)
//                ->where('active', 1);
//
//            if ($verify2->exists()) {
//                $token = random_int(100000, 999999);
//                $password_reset = LoginInformation::where('user_id', $information->id)->update([
//                    'verification_code' => $token,
//                    'created_at' => Carbon::now(),
//                ]);
//
//                if ($password_reset) {
//                    try {
//                        Mail::to($request->all()['email'])->send(new ResetPassword($token));
//                    }catch (\Exception $ex)
//                    {
//                        return redirect()->back()->with('fail','تحقق من شبكة الانترنيت');
//
//                    }
//                    $opLog = OperationLog::create([
//                        'operation_type' => 'نسيان كلمة المرور',
//                        'date' => Carbon::now(),
//                        'operation_details' => $request->all()['email'],
//                        'operation_result' => 'ارسال رمز التحقق الى البريد الالكتروني',
//                        'note' =>'',
//                        'user_id' => $information->id,
//                       'user_type' => $information->role
//                    ]);
//                    return redirect('resetPass')->with('success','يرجى التحقق من بريدك الإلكتروني للحصول على رقم تعريف شخصي مكون من 6 أرقام');
//                }else{
//                    return redirect()->back()->with('fail','حدث خطأ ما');
//
//                }
//            }else{
//                return redirect()->back()->with('fail','الحساب غير فعال');
//            }
//
//        } else {
//            return redirect()->back()->with('fail','البريد الالكتروني غير موجود');
//        }
//    }
//    public function resetPass(Request $request)
//    {
//        $validator = Validator::make($request->all(), [
//            'email' => ['required', 'string', 'email', 'max:255'],
//            'verification_code' => ['required'],
//            'password' => ['required','string', 'min:8','confirmed'],
//        ]);
//        if ($validator->fails()) {
//            return redirect('/resetPass')
//                ->withErrors($validator)
//                ->withInput();
//        }
//
//        $information = User::where('email', $request->all()['email'])->first();
//        if (!$information)
//            return redirect()->back()->with('fail','البريد الالكتروني غير موجود');
//        $check = LoginInformation::where([
//            ['user_id', $information->id],
//            ['verification_code', $request->all()['verification_code']],
//        ]);
//        if ($check->exists()) {
//            $difference = Carbon::now()->diffInSeconds($check->first()->created_at);
//            if ($difference > 3600) {
//                return redirect()->back()->with('fail','انتهت صلاحية ركز التحقق');
//            }
//
//            $information->update([
//                'password'=>Hash::make($request->password)
//            ]);
//            $opLog = OperationLog::create([
//                'operation_type' => 'تغيير كلمة مرور',
//                'date' => Carbon::now(),
//                'operation_details' => $request->all()['email'],
//                'operation_result' => 'تم تغيير كلمة المرور',
//                'note' =>'',
//                'user_id' => $information->id,
//                'user_type' => $information->role
//            ]);
//            $decRole = $this->binaryToDecimal($information->role);
//            if ($this->isKthBitSet($decRole, 1)) {
//                $opLog = new OperationLog;
//                $opLog->operation_type = 'تسجيل دخول';
//                $opLog->date = Carbon::now();
//                $opLog->operation_details = $request->email . ' :البريد الالكتروني';
//                $opLog->operation_result = 'تم تسجيل الدخول بنجاح';
//                $opLog->note = ' ';
//                $opLog->user_id = $information->id;
//                $opLog-> user_type = $information->role;
//                $opLog->save();
//                $credentials = $request->only('email', 'password');
//                Auth::guard('admin-api')->attempt($credentials);
//                return redirect()->route('admin.home');
//            } else if ($this->isKthBitSet($decRole, 2)) {
//                $opLog = new OperationLog;
//                $opLog->operation_type = 'تسجيل دخول';
//                $opLog->date = Carbon::now();
//                $opLog->operation_details = $request->email . ' :البريد الالكتروني';
//                $opLog->operation_result = 'تم تسجيل الدخول بنجاح';
//                $opLog->note = ' ';
//                $opLog->user_id = $information->id;
//                $opLog-> user_type = $information->role;
//                $opLog->save();
//                $credentials = $request->only('email', 'password');
//                Auth::guard('directoryManager-api')->attempt($credentials);
//                return redirect()->route('dir.home');
//            } else {
//                return redirect()->route('login')->with('fail', 'الدخول غير مسموح');
//            }
//
//        } else {
//            return redirect()->back()->with('fail','خطأ في رمز التحقق');
//        }
//    }
//
//
//    public function changePassword(Request $request)
//    {
//        $validator = Validator::make($request->all(), [
//            'old_password'=>['required'],
//            'password' => ['required','string','min:8','confirmed'],
//            'password_confirmation' => ['required','same:password','min:8'],
//        ]);
//        if ($validator->fails()) {
//            return redirect()
//                ->back()
//                ->withErrors($validator)
//                ->withInput();
//        }
//       $user=Auth::user();
//        if (Hash::check($request->old_password,$user->password))
//        {
//            User::where('id',$user->id)->update([
//                'password'=>Hash::make($request->password)
//            ]);
//        }
//        else{
//            return redirect()->back()->with('fail','كلمة المرور الحالية خاطئة');
//        }
//        $opLog = OperationLog::create([
//            'operation_type' => 'تغيير كلمة المرور',
//            'date' => Carbon::now(),
//            'operation_details' => $user->email,
//            'operation_result' => 'تم تغيير كلمة المرور',
//            'note' =>'',
//            'user_id' => $user->id,
//            'user_type' => $user->role,
//        ]);
//        return redirect()->back()->with('success','تم تغيير كلمة المرور بنجاح');
//    }

}
