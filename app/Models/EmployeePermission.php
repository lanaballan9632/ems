<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeePermission extends Model
{
    use HasFactory;
    protected $fillable = [
        'employee-id',
        'permission-id',
    ];
    public function permission(){
        return $this->belongsTo(Permission::class,'permission-id','id');
    }
    public function employee(){
        return $this->belongsTo(User::class,'employee-id','id');
    }
}
