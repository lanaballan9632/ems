<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SendInvitation extends Model
{
    use HasFactory;
    protected $fillable = [
        'surename',
        'surename2',
        'group',
        'name',
        'email',
        'extra_email',
        'destination',
        'number',
        'position',
        'lang',
        'send_email',
        'send_message',
        'confirm',
    ];
}
