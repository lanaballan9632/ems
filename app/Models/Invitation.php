<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    use HasFactory;
    protected $fillable = [
        'surename',
        'surename2',
        'group',
        'name',
        'email',
        'invitation_type',
        'destination',
        'number',
        'position',
        'seat_id',
         'attend',
    'original_id'
    ];
}
