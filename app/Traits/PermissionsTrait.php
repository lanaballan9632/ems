<?php

namespace App\Traits;

use App\Models\EmployeePermission;
use Illuminate\Support\Facades\Auth;

trait PermissionsTrait
{

    public function checkPermission($requestedId)
    {
       $permission=EmployeePermission::where('user_id',Auth::user()->id)->where('permission_id',$requestedId)->first();
       if($permission)
       {
           return true;
       }
       else{
           return false;
       }
    }
}
