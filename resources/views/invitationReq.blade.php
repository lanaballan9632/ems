<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>تسجيل طلب</title>
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@300;400;600&display=swap" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <!-- Include jQuery -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <!-- Include Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <style>
        .ar-font{
            font-family: 'Cairo', sans-serif;
        }
        .slate-blue-shadow {
            box-shadow: 0 0 5px slateblue;
        }
        .submit-btn,.submit-btn:hover{
            font-family: 'Cairo', sans-serif;
            background-color: #4F42B5;
            border-color: #4F42B5;
            border-radius: 20px;
            font-size: 18px;
            width: 90px;
        }
    </style>
</head>
<body class="antialiased bg-light">

<nav class="navbar navbar-expand-sm bg-white navbar-light pt-4 pb-4 pl-5 mt-0">
    <div class="container">
        <!-- Your navigation content -->
    </div>
</nav>

<div class="container mt-4 ar-font">
    @if(Session::get('fail'))
        <div style="background-color: red; color: white;text-align: right; padding: 10px"
             class="alert alert-danger">{{Session::get('fail')}}</div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card p-4 rounded-3 shadow">
                <div class="card-body">
                    <h2 class="card-title text-center" style="color: slateblue">سجل الآن</h2>
                    <form class="text-end" method="POST" role="form" action="/store/invitation/req">
                        @csrf
                    <div class="mb-3">
                        <label for="exampleSelect">اللقب</label>
                        <select name="surename" class="form-control shadow-sm" id="exampleSelect" style="" required>
                            @foreach($sureName as $surename)
                                <option class="text-end" value="{{$surename->id}}">{{$surename->name}}</option>
                            @endforeach
                        </select>
                    </div>
                        <div class="mb-3">
                            <label for="name" class="form-label">الاسم الكامل</label>
                            <input type="text" class="form-control text-end slate-blue-shadow text-end @error('name') is-invalid @enderror" id="name" name="name" required>

                            <span class="focus-input100" data-placeholder="&#xf207;"></span>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="number" class="form-label">رقم الجوال</label>
                            <input type="text" name="number" class="form-control slate-blue-shadow text-end @error('number') is-invalid @enderror" id="myInput" onclick="addPlace()" oninput="formatPhoneNumber(this)" maxlength="13" required>
                            <span class="focus-input100" data-placeholder="&#xf207;"></span>
                            @error('number')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="mb-3">
                            <label for="email" class="form-label">البريد الالكتروني</label>
                            <input type="email" class="form-control text-end slate-blue-shadow text-end @error('email') is-invalid @enderror" id="email" name="email" required >
                            <span class="focus-input100" data-placeholder="&#xf207;"></span>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="destination" class="form-label">الجهة</label>
                            <input type="text" class="form-control text-end slate-blue-shadow @error('destination') is-invalid @enderror"  id="destination" name="destination" required>
                            <span class="focus-input100" data-placeholder="&#xf207;"></span>
                            @error('destination')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="position" class="form-label">المنصب</label>
                            <input type="text" class="form-control text-end slate-blue-shadow @error('position') is-invalid @enderror"  id="position" name="position" required>
                            <span class="focus-input100" data-placeholder="&#xf207;"></span>
                            @error('position')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-primary submit-btn" id="submitForm">إرسال</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function addPlace() {
        var input = document.getElementById('myInput');
        input.placeholder = '+966---------';
    }
    function formatPhoneNumber(input) {
        var phoneNumber = input.value;
        if (!phoneNumber.startsWith('+966')) {
            input.value = '+966';
        }
        if (phoneNumber.length > 13) {
            input.value = phoneNumber.slice(0, 13);
        }

    }
</script>
</body>
</html>
