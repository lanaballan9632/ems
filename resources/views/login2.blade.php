<!DOCTYPE html>
<html lang="en">

<head>
    <base href="/public">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Responsive Admin &amp; Dashboard Template based on Bootstrap 5">
    <meta name="author" content="AdminKit">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    {{-- <link rel="shortcut icon" href="img/icons/icon-48x48.png" /> --}}
    <link rel="canonical" href="https://demo-basic.adminkit.io/pages-sign-in.html" />

    <title>تسجيل الدخول</title>

    <link href="css/app.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@300;400;600&display=swap" rel="stylesheet">

    <style>
        .form-control {
            border-radius: 25px;
            padding: 15px;
            font-size: 15px;
            transition: border-color 0.2s;

            box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);
        }

        .form-control.invalid {
            border-color: red;
        }

        .btn
        ,.btn:hover{
            border-radius: 20px;
            padding: 15px 30px;
            font-size: 15px;
            font-family: 'Cairo', sans-serif;
            background-color: #4F42B5;
            border-color: #4F42B5;
        }

        a ,a:hover{
color:  #4F42B5;
        }

.form-check-input:checked{
    border-color: #4F42B5;
    background-color: #4F42B5;
}
        label {
            font-size: 15px;
            font-family: 'Cairo', sans-serif;
        }
    </style>
</head>

<body>
<main class="d-flex w-100">
    <div class="container d-flex flex-column">
        <div class="row vh-100">
            <div class="col-sm-10 col-md-8 col-lg-6 col-xl-5 mx-auto d-table h-100">
                <div class="d-table-cell align-middle">

                    <div class="card">
                        <div class="card-body mt-4 mb-4">
                            <div class="m-sm-3">
                                <form method="POST" action="{{ route('check') }}" id="loginForm">
                                    @csrf
                                    @if(Session::get('fail'))
                                        <div style="background-color: red; color: white;text-align: right; padding: 10px"
                                             class="alert alert-danger">{{Session::get('fail')}}</div>
                                    @endif
                                    <div class="mb-3 text-end">
                                        <label style="@error('userName') color:red @enderror" class="form-label text-end ">* اسم المستخدم</label>
                                        <input class="form-control form-control-lg text-end @error('userName') is-invalid @enderror" type="text" name="userName" required />
                                        <span class="focus-input100" data-placeholder="&#xf207;"></span>
                                        @error('userName')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div class="mb-3 text-end">
                                        <label style="@error('password') color:red @enderror" class="form-label">* كلمة المرور</label>
                                        <input class="form-control form-control-lg text-end @error('password') is-invalid @enderror" type="password" name="password" required />
                                        <span class="focus-input100" data-placeholder="&#xf191;"></span>
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div>
                                        <div class="form-check d-flex justify-content-end">
                                            <label class="form-check-label text-small me-4" for="customControlInline">تذكرني</label>
                                            <input id="customControlInline" type="checkbox" class="form-check-input" name="remember" checked>
                                        </div>

                                    </div>
                                    <div class="d-grid gap-2 mt-3">
                                        <button class="btn btn-lg btn-primary">تسجيل الدخول</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</main>

<script src="js/app.js"></script>

</body>

</html>
