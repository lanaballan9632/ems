<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>لوحة التحكم</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@300;400;600&display=swap" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <!-- Include jQuery -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <!-- Include Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <!-- Include DataTables JS -->
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap4.min.js"></script>


    <!-- Include DataTables CSS -->
    <link href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap4.min.css" rel="stylesheet">

    <style>
        .navbar-nav .nav-link {
            margin-right: 10px;
            font-family: 'Cairo', sans-serif;
            font-size: 18px;
        }
        .user-n {
            font-size: 15px;
            font-family: 'Cairo', sans-serif;
        }
        .log-out-btn, .log-out-btn:hover {
            font-family: 'Cairo', sans-serif;
            background-color: #4F42B5;
            border-color: #4F42B5;
            border-radius: 0px;
            font-size: 15px;
        }
        .navbar.bg-light {
            margin-bottom: 0; /* Remove the bottom margin */
        }
        .navbar.bg-white {
            margin-top: -16px; /* Add a negative top margin */
        }
        .ar-font{
            font-family: 'Cairo', sans-serif;
        }
        .add {
            font-size: 17px;
            font-family: 'Cairo', sans-serif;
        }
        .close{
           color: #3a28c4;
            background-color: #4F42B5;
        }
        .submit-btn,.submit-btn:hover{
            font-family: 'Cairo', sans-serif;
            background-color: #4F42B5;
            border-color: #4F42B5;
            border-radius: 0px;
            font-size: 18px;
            width: 90px;
        }
    </style>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body class="antialiased bg-light">
<nav class="navbar navbar-expand-sm bg-light navbar-light justify-content-center mb-0">
    <form method="GET" action="{{ route('logout') }}">
        <button class="btn btn-lg btn-primary log-out-btn" type="submit">تسجيل الخروج</button>
    </form>
    <div class="mx-1">
        <span class="navbar-brand user-n">{{ auth()->user()->name }} مرحبا</span>
    </div>
</nav>
<nav class="navbar navbar-expand-sm bg-white navbar-light pt-4 pb-4 pl-5 mt-0">
    <div class="container">
        <div class="d-flex justify-content-between w-100">
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav pl-5">
                    <li class="nav-item">

                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">يوم الحفل</a>
                        <ul class="dropdown-menu text-end ar-font">
                            <li><a class="dropdown-item" href="/all/invitations">جميع الدعوات</a></li>
                            <li><a class="dropdown-item" href="/all/seats">الكراسي</a></li>
                            <li><a class="dropdown-item" href="/empty/seats">الكراسي الفارغة فقط</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">لوحة التحكم</a>
                        <ul class="dropdown-menu text-end ar-font">
                            <li><a class="dropdown-item" href="/send/invitation">إرسال الدعوات</a></li>
                            <li><a class="dropdown-item" href="/general/invitation">الدعوات العامة</a></li>
                            <li><a class="dropdown-item" href="/surename1">1الألقاب</a></li>
                            <li><a class="dropdown-item" href="/surename2">2الألقاب</a></li>
                            <li><a class="dropdown-item" href="/group">الفئات</a></li>
                            <li><a class="dropdown-item" href="/employee">إضافة موظفين</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </div>
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
        </div>
    </div>
</nav>
<script>
    $(document).ready(function() {
        $('.table').DataTable({
            language: {
                url: '//cdn.datatables.net/plug-ins/1.13.7/i18n/ar.json',
            },
        });
    });
</script>

@yield('content')
</body>
</html>
