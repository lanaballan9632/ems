@extends('master')
@section('content')
    <div class="container ar-font">

        <div class="mb-4 mt-5 text-end">
            @if(Session::get('success'))
                <div class="alert alert-success">{{Session::get('success')}}</div>
            @endif
            @if(Session::get('fail'))
                <div style="background-color: red; color: white;text-align: right; padding: 10px"
                     class="alert alert-danger">{{Session::get('fail')}}</div>
            @endif

            <a class="add" href="#" data-bs-toggle="modal" data-bs-target="#addEmployeeModal" style="color: slateblue;">
                إضافة
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-circle-fill" viewBox="0 0 16 16">
                    <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3z"/>
                </svg>
            </a>
        </div>

        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr class="text-end">
                <th></th>
                <th>الفئة</th>
                <th>م</th>
            </tr>
            </thead>
            <tbody>
            @foreach($groups as $group)
                <tr class="text-end">
                    <td>
                        <a class="add" href="#" style="color: slateblue;" onclick="populateModal( '{{ $group->name }}', '{{ $group->id }}')">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                                <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5z"/>
                            </svg>
                        </a>
                        <a class="add" href="/group/delete/{{$group->id}}"  style="color: slateblue;">

                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3-fill" viewBox="0 0 16 16">
                                <path d="M11 1.5v1h3.5a.5.5 0 0 1 0 1h-.538l-.853 10.66A2 2 0 0 1 11.115 16h-6.23a2 2 0 0 1-1.994-1.84L2.038 3.5H1.5a.5.5 0 0 1 0-1H5v-1A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5m-5 0v1h4v-1a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5M4.5 5.029l.5 8.5a.5.5 0 1 0 .998-.06l-.5-8.5a.5.5 0 1 0-.998.06Zm6.53-.528a.5.5 0 0 0-.528.47l-.5 8.5a.5.5 0 0 0 .998.058l.5-8.5a.5.5 0 0 0-.47-.528ZM8 4.5a.5.5 0 0 0-.5.5v8.5a.5.5 0 0 0 1 0V5a.5.5 0 0 0-.5-.5"/>
                            </svg>
                        </a> </td>
                    <td>{{ $group->name }}</td>
                    <td>{{ $group->id }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="modal fade" id="addEmployeeModal" tabindex="-1" aria-labelledby="addEmployeeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header ">
                    <h5 class="modal-title" id="addEmployeeModalLabel">إضافة</h5>
                    <button type="button" class="btn close" data-bs-dismiss="modal" >
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle-fill" viewBox="0 0 16 16">
                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293z"/>
                        </svg>
                    </button>
                </div>
                <div class="modal-body text-end ar-font">
                    @if(Session::get('fail'))
                        <div style="background-color: red; color: white;text-align: right; padding: 10px" class="alert alert-danger">{{Session::get('fail')}}</div>
                    @endif

                    <div style="background-color: slateblue; color: white;text-align: center; padding: 12px; font-size: 20px; border-radius: 5px" class="ar-font mb-4 mt-4 alert alert-danger">الفئة</div>
                    <form method="POST" role="form" action="/store/group">
                        @csrf

                        <div class="mb-3">
                            <input type="text" name="name" class="form-control text-end" id="name" placeholder="الفئة" required>
                            @error('name')
                            <span class="invalid-feedback" role="alert" style="color: red">
                <strong>{{ $message }}</strong>
            </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary submit-btn" id="submitForm">حفظ</button>
                    </form>

                    <div class="modal-footer">


                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="modal fade" id="editEmployeeModal" tabindex="-1" aria-labelledby="editEmployeeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header ">
                    <h5 class="modal-title" id="editEmployeeModalLabel">تعديل</h5>
                    <button type="button" class="btn close" data-bs-dismiss="modal" >
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle-fill" viewBox="0 0 16 16">
                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293z"/>
                        </svg>
                    </button>
                </div>
                <div class="modal-body text-end ar-font">
                    @if(Session::get('fail'))
                        <div style="background-color: red; color: white;text-align: right; padding: 10px" class="alert alert-danger">{{Session::get('fail')}}</div>
                    @endif

                    <div style="background-color: slateblue; color: white;text-align: center; padding: 12px; font-size: 20px; border-radius: 5px" class="ar-font mb-4 mt-4 alert alert-danger">الفئة</div>
                    <form method="POST" role="form" action="/update/group">
                        @csrf

                        <input type="hidden" name="groupId" id="groupId" value="">
                        <div class="mb-3">
                            <input type="text" name="name" class="form-control text-end" id="editedName" placeholder="الفئة" required value="">
                            @error('name')
                            <span class="invalid-feedback" role="alert" style="color: red">
                <strong>{{ $message }}</strong>
            </span>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-primary submit-btn" id="submitForm">حفظ</button>
                    </form>

                    <div class="modal-footer">


                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function populateModal(name, id) {
            var modal = new bootstrap.Modal(document.getElementById('editEmployeeModal'));
            document.getElementById('groupId').value = id;
            document.getElementById('editedName').value = name;
            modal.show();
        }
    </script>


@endsection
