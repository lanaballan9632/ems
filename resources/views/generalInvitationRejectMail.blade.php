<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@300;400;600&display=swap" rel="stylesheet">
    <title>Invitation</title>
    <style>
        .ar-font{
            font-family: 'Cairo', sans-serif;
        }
    </style>
</head>
<body class="ar-font">
@if(isset($mailData['surename']))
    <h2 style="text-align: center; font-weight: bold;">
        {{ $mailData['surename'] }} {{ $mailData['surename2'] }}: {{ $mailData['name'] }}
    </h2>
@else
    <h2 style="text-align: center; font-weight: bold;">
        {{ $mailData['surename2'] }}: {{ $mailData['name'] }}
    </h2>

@endif
<h3 style="text-align: center">نشكر لكم اهتمامكم وتسجليكم لحضور الفعالية. ونود الاعتذار عن قبول طلب التسجيل بسبب محدودية عدد المقاعد.</h3>
<h2 style="text-align: center">كما يسرنا انضمامكم للبث المباشر للجلسة عبر حسابات الرامج في مواقع التواصل الاجتماعي</h2>

</body>
</html>
