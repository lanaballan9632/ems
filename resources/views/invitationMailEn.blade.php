<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@300;400;600&display=swap" rel="stylesheet">
    <title>Invitation</title>
    <style>

        .sub-a
        {
            text-decoration: none;
            background-color: slateblue;
            color: white;
            border-radius: 20px;
            padding: 10px 15px 10px 15px;
            width: 50px;
            font-size: 18px;
        }
    </style>
</head>
<body class="ar-font">
<h2 style="text-align: center; font-weight: bold;">
   {{ $mailData['name'] }} :{{ $mailData['surename2'] }} {{ $mailData['surename'] }}
</h2>
<h3>We are pleased to invite you to attend an event</h3>
<div class="mb-3" style="text-align: center;">
    <div class="row mb-4">
        <p>To Confirm Attendance</p>
    </div>
    <a class="sub-a" href="http://localhost:8000/confirm/mail/{{$mailData['invitation_id']}}">Click Here</a>
</div>

</body>
</html>
