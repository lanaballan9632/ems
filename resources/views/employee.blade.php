@extends('master')
@section('content')
    <div class="container ar-font">

        <div class="mb-4 mt-5 text-end">
            @if(Session::get('success'))
                <div class="alert alert-success">{{Session::get('success')}}</div>
            @endif
                @if(Session::get('fail'))
                    <div style="background-color: red; color: white;text-align: right; padding: 10px"
                         class="alert alert-danger">{{Session::get('fail')}}</div>
                @endif
                @error('email')
                    <div style="background-color: red; color: white;text-align: right; padding: 10px"
                         class="alert alert-danger">{{ $message }}</div>
                @enderror
                @error('userName')
                <div style="background-color: red; color: white;text-align: right; padding: 10px"
                     class="alert alert-danger">{{ $message }}</div>
                @enderror
            <a class="add" href="#" data-bs-toggle="modal" data-bs-target="#addEmployeeModal" style="color: slateblue;">
                إضافة
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-circle-fill" viewBox="0 0 16 16">
                    <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3z"/>
                </svg>
            </a>
        </div>

        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr class="text-end">
                <th></th>
                <th>البريد الالكتروني</th>
                <th>اسم الموظف</th>
                <th>م</th>
            </tr>
            </thead>
            <tbody>
            @foreach($employees as $employee)
                <tr class="text-end">
                    <td>
                        <a class="add edit-employee" href="#"  data-employee="{{ json_encode($employee) }}"  style="color: slateblue;">

                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-fill-lock" viewBox="0 0 16 16">
                                <path d="M11 5a3 3 0 1 1-6 0 3 3 0 0 1 6 0m-9 8c0 1 1 1 1 1h5v-1a1.9 1.9 0 0 1 .01-.2 4.49 4.49 0 0 1 1.534-3.693C9.077 9.038 8.564 9 8 9c-5 0-6 3-6 4m7 0a1 1 0 0 1 1-1v-1a2 2 0 1 1 4 0v1a1 1 0 0 1 1 1v2a1 1 0 0 1-1 1h-4a1 1 0 0 1-1-1zm3-3a1 1 0 0 0-1 1v1h2v-1a1 1 0 0 0-1-1"/>
                            </svg>
                        </a>
                        <a class="add edit-btn" href="#" data-name="{{ $employee->name }}"  data-email="{{ $employee->email}}"
                           data-username="{{ $employee->userName}}" data-id="{{ $employee->id}}"
                           style="color: slateblue;">

                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                                <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5z"/>
                            </svg>
                        </a>
                        <a class="add" href="/employee/delete/{{$employee->id}}"  style="color: slateblue;">

                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3-fill" viewBox="0 0 16 16">
                                <path d="M11 1.5v1h3.5a.5.5 0 0 1 0 1h-.538l-.853 10.66A2 2 0 0 1 11.115 16h-6.23a2 2 0 0 1-1.994-1.84L2.038 3.5H1.5a.5.5 0 0 1 0-1H5v-1A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5m-5 0v1h4v-1a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5M4.5 5.029l.5 8.5a.5.5 0 1 0 .998-.06l-.5-8.5a.5.5 0 1 0-.998.06Zm6.53-.528a.5.5 0 0 0-.528.47l-.5 8.5a.5.5 0 0 0 .998.058l.5-8.5a.5.5 0 0 0-.47-.528ZM8 4.5a.5.5 0 0 0-.5.5v8.5a.5.5 0 0 0 1 0V5a.5.5 0 0 0-.5-.5"/>
                            </svg>
                        </a> </td>
                    <td>{{ $employee->email }}</td>
                    <td>{{ $employee->name }}</td>
                    <td>{{ $employee->id }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <!-- Add Employee Modal -->
    <div class="modal fade" id="addEmployeeModal" tabindex="-1" aria-labelledby="addEmployeeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header ">
                    <h5 class="modal-title" id="addEmployeeModalLabel">إضافة</h5>
                    <button type="button" class="btn close" data-bs-dismiss="modal" >
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle-fill" viewBox="0 0 16 16">
                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293z"/>
                        </svg>
                    </button>
                </div>
                <div class="modal-body text-end ar-font">
                    @if(Session::get('fail'))
                        <div style="background-color: red; color: white;text-align: right; padding: 10px"
                             class="alert alert-danger">{{Session::get('fail')}}</div>
                    @endif
                    <div style="background-color:  slateblue; color: white;text-align: center; padding: 12px; font-size: 20px; border-radius: 5px" class="ar-font mb-4 mt-4"
                         class="alert alert-danger">الموظف</div>
                        <form method="POST" role="form" action="/store/employee" id="employeeForm">
                            @csrf
                            <div class="mb-3">
                                <label for="name" class="form-label">اسم الموظف<svg xmlns="http://www.w3.org/2000/svg" width="12" height="16" fill="currentColor" class="bi bi-person-fill" viewBox="0 0 16 16">
                                        <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6"/>
                                    </svg></label>
                                <input type="text" name="name" class="form-control text-end" id="name" required>
                                @error('name')
                                <span class="invalid-feedback" role="alert" style="color: red">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="email" class="form-label">البريد الالكتروني<svg xmlns="http://www.w3.org/2000/svg" width="10" height="16" fill="currentColor" class="bi bi-envelope-fill" viewBox="0 0 16 16">
                                        <path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555ZM0 4.697v7.104l5.803-3.558zM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757Zm3.436-.586L16 11.801V4.697l-5.803 3.546Z"/>
                                    </svg></label>
                                <input type="email" name="email" class="form-control text-end" id="email" required>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="userName" class="form-label">اسم المستخدم<svg xmlns="http://www.w3.org/2000/svg" width="12" height="16" fill="currentColor" class="bi bi-person-fill" viewBox="0 0 16 16">
                                        <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6"/>
                                    </svg></label>
                                <input type="text" class="form-control text-end" id="userName" name="userName" required>
                                @error('userName')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="password" class="form-label">كلمة المرور<svg xmlns="http://www.w3.org/2000/svg" width="10" height="16" fill="currentColor" class="bi bi-star-fill" viewBox="0 0 16 16">
                                        <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                    </svg></label>
                                <input type="password" class="form-control text-end" id="password" name="password" required minlength="8">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-primary submit-btn" id="submitForm">حفظ</button>
                        </form>
                </div>
                <div class="modal-footer">


                </div>
            </div>
        </div>
    </div>

{{-- edit Employee modal--}}
    <div class="modal fade" id="editEmployee" tabindex="-1" aria-labelledby="addEmployeeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header ">
                    <h5 class="modal-title" id="addEmployeeModalLabel">تحريح</h5>
                    <button type="button" class="btn close" data-bs-dismiss="modal" >
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle-fill" viewBox="0 0 16 16">
                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293z"/>
                        </svg>
                    </button>
                </div>
                <div class="modal-body text-end ar-font">
                    @if(Session::get('fail'))
                        <div style="background-color: red; color: white;text-align: right; padding: 10px"
                             class="alert alert-danger">{{Session::get('fail')}}</div>
                    @endif
                    <div style="background-color:  slateblue; color: white;text-align: center; padding: 12px; font-size: 20px; border-radius: 5px" class="ar-font mb-4 mt-4"
                         class="alert alert-danger">الموظف</div>
                    <form method="POST" role="form" action="/update/employee" id="employeeForm">
                        @csrf
                        <input type="hidden" name="id" id="editId" value="">
                        <div class="mb-3">
                            <label for="name" class="form-label">اسم الموظف<svg xmlns="http://www.w3.org/2000/svg" width="12" height="16" fill="currentColor" class="bi bi-person-fill" viewBox="0 0 16 16">
                                    <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6"/>
                                </svg></label>
                            <input type="text" name="name" class="form-control text-end" id="editName" required>
                            @error('name')
                            <span class="invalid-feedback" role="alert" style="color: red">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">البريد الالكتروني<svg xmlns="http://www.w3.org/2000/svg" width="10" height="16" fill="currentColor" class="bi bi-envelope-fill" viewBox="0 0 16 16">
                                    <path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555ZM0 4.697v7.104l5.803-3.558zM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757Zm3.436-.586L16 11.801V4.697l-5.803 3.546Z"/>
                                </svg></label>
                            <input type="email" name="email" class="form-control text-end" id="editemail" required>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="userName" class="form-label">اسم المستخدم<svg xmlns="http://www.w3.org/2000/svg" width="12" height="16" fill="currentColor" class="bi bi-person-fill" viewBox="0 0 16 16">
                                    <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6"/>
                                </svg></label>
                            <input type="text" class="form-control text-end" id="editUserName" name="userName" required>
                            @error('userName')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>


                        <button type="submit" class="btn btn-primary submit-btn" id="submitForm">حفظ</button>
                    </form>
                </div>
                <div class="modal-footer">


                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addEmployeeModal2" tabindex="-1" aria-labelledby="addEmployeeModalLabel2" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header ">
                    <h5 class="modal-title" id="addEmployeeModalLabel2">الصلاحيات</h5>
                    <button type="button" class="btn close" data-bs-dismiss="modal" >
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle-fill" viewBox="0 0 16 16">
                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293z"/>
                        </svg>
                    </button>
                </div>
                <div class="modal-body text-end ar-font">
                    @if(Session::get('fail'))
                        <div style="background-color: red; color: white;text-align: right; padding: 10px"
                             class="alert alert-danger">{{Session::get('fail')}}</div>
                    @endif
                    <div style="background-color:  slateblue; color: white;text-align: center; padding: 12px; font-size: 20px; border-radius: 5px" class="ar-font mb-4 mt-4"
                         class="alert alert-danger">الصلاحيات</div>
                    <form method="POST" role="form"  id="perForm">
                        @csrf
                        <div class="row mb-5">
                            @foreach($permissions as $permission)
                                <div class="col-md-3 col-lg-3 checkbox-row">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="permissions[]"
                                               value="{{ $permission->id }}" id="{{ $permission->id }}"
                                              >
                                        <label class="form-check-label" for="{{ $permission->id }}">
                                            {{ $permission->permission_name }}
                                        </label>
                                    </div>
                                </div>
                            @endforeach
                        </div>


                        <button type="submit" class="btn btn-primary submit-btn" id="submitForm">حفظ</button>
                    </form>
                </div>
                <div class="modal-footer">


                </div>
            </div>
        </div>
    </div>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            var editButtons = document.querySelectorAll('.edit-employee');

            editButtons.forEach(function (button) {
                button.addEventListener('click', function () {
                    var employeeData = JSON.parse(this.getAttribute('data-employee'));
                    populateEditModal(employeeData);
                });
            });

            function populateEditModal(employeeData) {
                var modal = new bootstrap.Modal(document.getElementById('addEmployeeModal2'));
                var checkboxes = modal._element.querySelectorAll('input[type="checkbox"]');
                var form = document.getElementById('perForm');

                checkboxes.forEach(function (checkbox) {
                    var permissionId = parseInt(checkbox.value);
                    var hasPermission = employeeData.permissions.some(function (permission) {
                        return permission.id === permissionId;
                    });

                    checkbox.checked = hasPermission;
                });

                // Set the employee ID in the form action attribute
                form.action = "/update/permission/" + employeeData.id;

                // Show the modal
                modal.show();
            }
        });
    </script>


    <script>
        $(document).ready(function(){
            $('.edit-btn').click(function(){
                var userName = $(this).data('username');
                var userEmail = $(this).data('email');
                var name = $(this).data('name');
                var id = $(this).data('id');

                $('#editId').val(id);
                $('#editName').val(name);
                $('#editemail').val(userEmail);
                $('#editUserName').val(userName);
                $('#editEmployee').modal('show');
            });
        });

    </script>


@endsection
