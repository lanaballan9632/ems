@extends('master')
@section('content')
    <div class="container ar-font">

        <div class="mb-4 mt-5 text-end">
            @if(Session::get('success'))
                <div class="alert alert-success">{{Session::get('success')}}</div>
            @endif
            @if(Session::get('fail'))
                <div style="background-color: red; color: white;text-align: right; padding: 10px"
                     class="alert alert-danger">{{Session::get('fail')}}</div>
            @endif

            <a class="add" href="#" data-bs-toggle="modal" data-bs-target="#addEmployeeModal" style="color: slateblue;">
                إضافة
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-circle-fill" viewBox="0 0 16 16">
                    <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3z"/>
                </svg>
            </a>
        </div>

        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr class="text-end">
                <th></th>
                <th>حالة الكرسي</th>
                <th>فئة الكرسي</th>
                <th>المدعو</th>
                <th>رمز الكرسي</th>
                <th>م</th>
            </tr>
            </thead>
            <tbody>
            @foreach($seats as $seat)
                <tr class="text-end">
                    <td>
                        <a class="add" href="#" style="color: slateblue;" onclick="populateModal('{{ $seat->type }}', '{{ $seat->person_name }}', '{{ $seat->id }}', '{{ $seat->seat_id }}')">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                                <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5z"/>
                            </svg>
                        </a>
                       </td>
                    <td>{{ $seat->status }}</td>
                    <td>{{ $seat->type }}</td>
                    <td>{{ $seat->person_name }}</td>
                    <td>{{ $seat->seat_id }}</td>
                    <td>{{ $seat->id }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="modal fade" id="editEmployeeModal" tabindex="-1" aria-labelledby="editEmployeeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header ">
                    <h5 class="modal-title" id="editEmployeeModalLabel">تحريح</h5>
                    <button type="button" class="btn close" data-bs-dismiss="modal" >
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle-fill" viewBox="0 0 16 16">
                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293z"/>
                        </svg>
                    </button>
                </div>
                <div class="modal-body text-end ar-font">
                    @if(Session::get('fail'))
                        <div style="background-color: red; color: white;text-align: right; padding: 10px" class="alert alert-danger">{{Session::get('fail')}}</div>
                    @endif


                    <form method="POST" role="form" action="/update/seat">
                        @csrf

                        <input type="hidden" name="id" id="seatId" value="">
                        <div class="mb-3">
                            <div class="row mb-2">
                                <span>رمز الكرسي</span>

                            </div>
                            <span id="editSeatId"></span>
                        </div>
                        <div class="mb-3">

                            <label for="exampleSelect">المدعو</label>
                            <select name="person" class="form-control shadow-sm" id="person" style="">
                                @foreach($names as $name)
                                    <option value="{{$name->name}}">{{$name->name}}</option>
                                @endforeach
                            </select>

                        </div>
                        <div class="mb-3">
                            <div class="row mb-2">
                                <span>فئة الكرسي</span>

                            </div>
                            <span id="editSeatType"></span>
                        </div>
                        <button type="submit" class="btn btn-primary submit-btn" id="submitForm">حفظ</button>
                    </form>

                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>

        function populateModal(type, person_name, id,seat_id) {
            var modal = new bootstrap.Modal(document.getElementById('editEmployeeModal'));
            document.getElementById('seatId').value = id;
            document.getElementById('person').value = person_name;
            document.getElementById('editSeatId').innerText = seat_id;
            document.getElementById('editSeatType').innerText = type;

            modal.show();
        }





    </script>


@endsection
