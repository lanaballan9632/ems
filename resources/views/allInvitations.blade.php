@extends('master')
@section('content')
    <div class="container ar-font">

        <div class="mb-4 mt-5 text-end">
            @if(Session::get('success'))
                <div class="alert alert-success">{{Session::get('success')}}</div>
            @endif
            @if(Session::get('fail'))
                <div style="background-color: red; color: white;text-align: right; padding: 10px"
                     class="alert alert-danger">{{Session::get('fail')}}</div>
            @endif
            @error('email')
            <div style="background-color: red; color: white;text-align: right; padding: 10px"
                 class="alert alert-danger">{{ $message }}</div>
            @enderror
                @error('name')
                <div style="background-color: red; color: white;text-align: right; padding: 10px"
                     class="alert alert-danger">{{ $message }}</div>
                @enderror
            <a class="add" href="#" data-bs-toggle="modal" data-bs-target="#addEmployeeModal" style="color: slateblue;">
                إضافة
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus-circle-fill" viewBox="0 0 16 16">
                    <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M8.5 4.5a.5.5 0 0 0-1 0v3h-3a.5.5 0 0 0 0 1h3v3a.5.5 0 0 0 1 0v-3h3a.5.5 0 0 0 0-1h-3z"/>
                </svg>
            </a>
        </div>

        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr class="text-end">
                <th></th>
                <th>هل حضر الحفل</th>
                <th>رمز المقعد</th>
                <th>نوع الدعوة</th>
                <th>البريد الالكتروني</th>
                <th>رقم الجوال</th>
                <th>الاسم</th>



            </tr>
            </thead>
            <tbody>
            @foreach($invitations as $invitation)
                <tr class="text-end">
                    <td>
                        <a class="add" href="/delete/confirmed/invitation/{{$invitation->id}}"  style="color: slateblue;">

                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3-fill" viewBox="0 0 16 16">
                                <path d="M11 1.5v1h3.5a.5.5 0 0 1 0 1h-.538l-.853 10.66A2 2 0 0 1 11.115 16h-6.23a2 2 0 0 1-1.994-1.84L2.038 3.5H1.5a.5.5 0 0 1 0-1H5v-1A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5m-5 0v1h4v-1a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5M4.5 5.029l.5 8.5a.5.5 0 1 0 .998-.06l-.5-8.5a.5.5 0 1 0-.998.06Zm6.53-.528a.5.5 0 0 0-.528.47l-.5 8.5a.5.5 0 0 0 .998.058l.5-8.5a.5.5 0 0 0-.47-.528ZM8 4.5a.5.5 0 0 0-.5.5v8.5a.5.5 0 0 0 1 0V5a.5.5 0 0 0-.5-.5"/>
                            </svg>
                        </a>

                        <a class="add seat-btn" href="#" data-id="{{ $invitation->id}}"  data-seat="{{ $invitation->seat_id}}" style="color: slateblue;">

                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-fill" viewBox="0 0 16 16">
                                <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3Zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6Z"/>
                            </svg>
                        </a>

                        <a class="add edit-btn" href="#" data-name="{{ $invitation->name }}"  data-email="{{ $invitation->email}}"
                           data-destination="{{ $invitation->destination}}" data-number="{{ $invitation->number}}"
                           data-position="{{ $invitation->position}}" data-id="{{ $invitation->id}}"

                           data-confirm="{{ $invitation->attend}}" data-surename="{{ $invitation->surename}}"
                           data-surename2="{{ $invitation->surename2}}" data-group="{{ $invitation->group}}"

                           style="color: slateblue;">

                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                                <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5z"/>
                            </svg>
                        </a>

                        <a class="add details-btn" href="#" data-name="{{ $invitation->name }}"  data-email="{{ $invitation->email}}" data-destination="{{ $invitation->destination}}" data-number="{{ $invitation->number}}" data-position="{{ $invitation->position}}"  style="color: slateblue;">

                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye-fill" viewBox="0 0 16 16">
                                <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0"/>
                                <path d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8m8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7"/>
                            </svg>
                        </a></td>

                    <td>@if ($invitation->attend )
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check2" viewBox="0 0 16 16">
                                <path d="M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0z"/>
                            </svg>
                        @else
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16">
                                <path d="M2.146 2.854a.5.5 0 1 1 .708-.708L8 7.293l5.146-5.147a.5.5 0 0 1 .708.708L8.707 8l5.147 5.146a.5.5 0 0 1-.708.708L8 8.707l-5.146 5.147a.5.5 0 0 1-.708-.708L7.293 8z"/>
                            </svg>
                        @endif</td>

                    <td>{{ $invitation->seat_id }}</td>
                    <td>{{ $invitation->invitation_type }}</td>
                    <td>{{ $invitation->email }}</td>
                    <td>{{ $invitation->number }}</td>
                    <td>{{ $invitation->name }}</td>

                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <!-- Add Employee Modal -->
    <div class="modal fade" id="addEmployeeModal" tabindex="-1" aria-labelledby="addEmployeeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header ">
                    <h5 class="modal-title" id="addEmployeeModalLabel">إضافة</h5>
                    <button type="button" class="btn close" data-bs-dismiss="modal" >
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle-fill" viewBox="0 0 16 16">
                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293z"/>
                        </svg>
                    </button>
                </div>
                <div class="modal-body text-end ar-font">
                    @if(Session::get('fail'))
                        <div style="background-color: red; color: white;text-align: right; padding: 10px"
                             class="alert alert-danger">{{Session::get('fail')}}</div>
                    @endif
                    <div style="background-color:  slateblue; color: white;text-align: center; padding: 12px; font-size: 20px; border-radius: 5px" class="ar-font mb-4 mt-4"
                         class="alert alert-danger">معلومات المدعو</div>
                    <form method="POST" role="form" action="/store/confirmed/invitation" id="employeeForm">
                        @csrf
                        <div class="row mb-3">
                            <div class="col-6">
                                <label for="exampleSelect">اللقب2</label>
                                <select name="surename2" class="form-control shadow-sm" id="exampleSelect" style="">
                                    @foreach($sureName2 as $surename)
                                        <option value="{{$surename->id}}">{{$surename->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-6">
                                @if(!empty($sureName) && count($sureName) > 0)
                                    <input type="hidden" name="surename" id="selectSureName1" value="{{$sureName[0]->id}}">
                                    <label for="exampleSelect">اللقب</label>
                                    <div class="row mb-4">
                                        @foreach($sureName as $key => $surename)
                                            <div class="col-3 mb-3" style="cursor: pointer;" onclick="selectSureName('{{$surename->name}}','{{$surename->id}}')">
                                                <div id="{{$surename->name}}" class="surename-option" style="width: 70px; height: 50px; background-color: lightgray; color: black; text-align: center; line-height: 50px; {{$key === 0 ? 'background-color: slateblue; color: white;' : ''}}">{{$surename->name}}</div>
                                            </div>
                                        @endforeach
                                    </div>
                                @else
                                    <input type="hidden" name="surename" id="selectSureName1" value="0">
                                    <label for="exampleSelect">اللقب</label>
                                @endif
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-6">
                                <label for="number" class="form-label">رقم الجوال</label>
                                <input type="text" name="number" class="form-control text-end" id="myInput" onclick="addPlace()" oninput="formatPhoneNumber(this)" maxlength="13" required>
                                @error('number')
                                <span class="invalid-feedback" role="alert" style="color: red">
        <strong>{{ $message }}</strong>
    </span>
                                @enderror
                            </div>
                            <div class="col-6">
                                <label for="name" class="form-label">الاسم الكامل</label>
                                <input type="text" name="name" class="form-control text-end" id="name" required>
                                @error('name')
                                <span class="invalid-feedback" role="alert" style="color: red">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>

                        </div>
                        <div class="row mb-3">
                            <div class="col-6">
                                <label for="destination" class="form-label">الجهة</label>
                                <input type="text" name="destination" class="form-control text-end" id="destination" required>
                                @error('destination')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                            <div class="col-6">
                                <label for="email" class="form-label">البريد الالكتروني</label>
                                <input type="email" name="email" class="form-control text-end" id="email" required>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-6">
                            </div>
                            <div class="col-6">

                                <label for="position" class="form-label">المنصب</label>
                                <input type="text" name="position" class="form-control text-end" id="position" required>
                                @error('position')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>


                        </div>
                        <div class="row mb-3">
                            <div class="col-6">

                            </div>
                            <div class="col-6">
                                <label for="group" class="form-label">الفئة</label>
                                <select name="group" class="form-control shadow-sm" id="exampleSelect" style="">
                                    @foreach($groups as $group)
                                        <option value="{{$group->id}}">{{$group->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                        <div class="row mb-3">
                            <div class="col-6">

                            </div>
                            <div class="col-6">
                                <label for="confirm" class="form-label">هل حضر الحفل</label>
                                <input type="hidden" name="confirm" id="confirm" value="false">
                                <div class="mb-4" style="display: flex; justify-content: end; align-items: end;">

                                    <div style=" cursor: pointer;" onclick="selectConfirm('true')">
                                        <div id="trueConfirm" style="width: 50px; height: 50px; background-color: lightgray; color: black; text-align: center; line-height: 50px;">نعم</div>
                                    </div>
                                    <div style=" cursor: pointer;" onclick="selectConfirm('false')">
                                        <div id="falseConfirm" style="width: 50px; height: 50px; background-color: red; color: white; text-align: center; line-height: 50px;">لا</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary submit-btn" id="submitForm">إرسال</button>
                    </form>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="userDetailsModal" tabindex="-1" aria-labelledby="userDetailsModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header ">
                    <h5 class="modal-title" id="userDetailsModal">التفاصيل</h5>
                    <button type="button" class="btn close" data-bs-dismiss="modal" >
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle-fill" viewBox="0 0 16 16">
                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293z"/>
                        </svg>
                    </button>
                </div>
                <div class="modal-body text-end ar-font">


                    <div style="background-color: slateblue; color: white;text-align: center; padding: 12px; font-size: 20px; border-radius: 5px" class="ar-font mb-4 mt-4 alert alert-danger">تفاصيل الدعوة</div>
                    <table class="table table-striped table-bordered">

                        <tbody>
                        <tr>
                            <td>الاسم: <span id="modalUserName"></span></td>
                        </tr>
                        <tr>
                            <td>رقم الواتس آب: <span id="modalUserNumber"></span></td>
                        </tr>
                        <tr>
                            <td>البريد الالكتروني: <span id="modalUserEmail"></span></td>
                        </tr>
                        <tr>
                            <td>المنصب: <span id="modalUserPosition"></span></td>
                        </tr>
                        <tr>
                            <td>الجهة: <span id="modalUserDestination"></span></td>
                        </tr>

                        </tbody>

                    </table>

                    <div class="modal-footer">


                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="editInvitation" tabindex="-1" aria-labelledby="addEmployeeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header ">
                    <h5 class="modal-title" id="addEmployeeModalLabel">تحريح</h5>
                    <button type="button" class="btn close" data-bs-dismiss="modal" >
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle-fill" viewBox="0 0 16 16">
                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293z"/>
                        </svg>
                    </button>
                </div>
                <div class="modal-body text-end ar-font">
                    @if(Session::get('fail'))
                        <div style="background-color: red; color: white;text-align: right; padding: 10px"
                             class="alert alert-danger">{{Session::get('fail')}}</div>
                    @endif
                    <div style="background-color:  slateblue; color: white;text-align: center; padding: 12px; font-size: 20px; border-radius: 5px" class="ar-font mb-4 mt-4"
                         class="alert alert-danger">معلومات المدعو</div>
                    <form method="POST" role="form" action="/update/confirmed/invitation" >
                        @csrf
                        <input type="hidden" name="id" id="editId" value="">
                        <div class="row mb-3">
                            <div class="col-6">
                                <label for="exampleSelect">اللقب2</label>
                                <select name="surename2" class="form-control shadow-sm" id="editSureName2" style="">
                                    @foreach($sureName2 as $surename)
                                        <option value="{{$surename->id}}">{{$surename->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-6">
                                @if(!empty($sureName) && count($sureName) > 0)

                                    <input type="hidden" name="surename" id="editSureName1" value="">

                                    <label for="exampleSelect">اللقب</label>
                                    <div class="row mb-4">
                                        @foreach($sureName as $key => $surenameOption)
                                            <div class="col-3 mb-3" style="cursor: pointer;" onclick="selectEditSureName('{{$surenameOption->name}}','{{$surenameOption->id}}')">
                                                <div id="edit{{$surenameOption->id}}" class="editsurename-option" style="width: 70px; height: 50px; text-align: center; line-height: 50px;
                                                    background-color: lightgray; color: black;
                                             ">{{ $surenameOption->name }}</div>
                                            </div>
                                        @endforeach
                                    </div>
                                @else
                                    <input type="hidden" name="surename" value="0">
                                    <label for="exampleSelect">اللقب</label>
                                @endif
                            </div>


                        </div>
                        <div class="row mb-3">
                            <div class="col-6">

                                <label for="number" class="form-label">رقم الجوال</label>
                                <input type="text" name="number" class="form-control text-end" id="editmyInput" onclick="editaddPlace()" oninput="editformatPhoneNumber(this)" maxlength="13" required>
                                @error('number')
                                <span class="invalid-feedback" role="alert" style="color: red">
        <strong>{{ $message }}</strong>
    </span>
                                @enderror


                            </div>
                            <div class="col-6">
                                <label for="name" class="form-label">الاسم الكامل</label>
                                <input type="text" name="name" class="form-control text-end" id="editname" required>
                                @error('name')
                                <span class="invalid-feedback" role="alert" style="color: red">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>

                        </div>


                        <div class="row mb-3">
                            <div class="col-6">
                                <label for="destination" class="form-label">الجهة</label>
                                <input type="text" name="destination" class="form-control text-end" id="editdestination" required>
                                @error('destination')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                            <div class="col-6">
                                <label for="email" class="form-label">البريد الالكتروني</label>
                                <input type="email" name="email" class="form-control text-end" id="editemail" required>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>

                        </div>
                        <div class="row mb-3">
                            <div class="col-6">

                            </div>
                            <div class="col-6">

                                <label for="position" class="form-label">المنصب</label>
                                <input type="text" name="position" class="form-control text-end" id="editposition" required>
                                @error('position')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror

                            </div>

                        </div>
                        <div class="row mb-3">
                            <div class="col-6">

                            </div>
                            <div class="col-6">
                                <label for="group" class="form-label">الفئة</label>
                                <select name="group" class="form-control shadow-sm" id="editgroup" style="">
                                    @foreach($groups as $group)
                                        <option value="{{$group->id}}">{{$group->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                        <div class="row mb-3">

                            <div class="col-6">

                            </div>
                            <div class="col-6">
                                <label for="confirm" class="form-label">هل حضر الحفل</label>
                                <input type="hidden" name="confirm" id="editconfirm" value="false">
                                <div id="div-cc" class="mb-4" style="display: flex; justify-content: end; align-items: end;" data-confirm="">
                                    <div style=" cursor: pointer;" onclick="editselectConfirm('false')">
                                        <div id="editfalseConfirm" style="width: 50px; height: 50px; text-align: center; line-height: 50px;

                                        background-color: lightgray; color: black;

                                        ">لا</div>
                                    </div>
                                    <div style=" cursor: pointer;" onclick="editselectConfirm('true')">
                                        <div id="edittrueConfirm" style="width: 50px; height: 50px; text-align: center; line-height: 50px;
                                        background-color: lightgray; color: black;


">نعم</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary submit-btn" id="submitForm">إرسال</button>
                    </form>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="seatModal" tabindex="-1" aria-labelledby="addEmployeeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header ">
                    <h5 class="modal-title" id="addEmployeeModalLabel">تعديل المقعد</h5>
                    <button type="button" class="btn close" data-bs-dismiss="modal" >
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle-fill" viewBox="0 0 16 16">
                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M5.354 4.646a.5.5 0 1 0-.708.708L7.293 8l-2.647 2.646a.5.5 0 0 0 .708.708L8 8.707l2.646 2.647a.5.5 0 0 0 .708-.708L8.707 8l2.647-2.646a.5.5 0 0 0-.708-.708L8 7.293z"/>
                        </svg>
                    </button>
                </div>
                <div class="modal-body text-end ar-font">
                    @if(Session::get('fail'))
                        <div style="background-color: red; color: white;text-align: right; padding: 10px"
                             class="alert alert-danger">{{Session::get('fail')}}</div>
                    @endif

                    <form method="POST" role="form" action="/set/seat" id="employeeForm">
                        @csrf
                        <input type="hidden" name="invitationId" id="seatInvId" value="">
                        <div class="mb-3">

                                <label for="exampleSelect">رمز المقعد</label>
                                <select name="seatId" class="form-control shadow-sm" id="seatId" style="">
                                    @foreach($seats as $seat)
                                        <option value="{{$seat->seat_id}}">{{$seat->seat_id}}</option>
                                    @endforeach
                                </select>

                        </div>

                        <button type="submit" class="btn btn-primary submit-btn" id="submitForm">حفظ</button>
                    </form>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            // Handle click on "edit" link
            $('.table tbody').on('click', '.edit', function () {
                var row = $(this).closest('tr');
                var employee_id = row.find('td:last').text();
                var name = row.find('td:eq(2)').text();
                var email = row.find('td:eq(1)').text();
                var userName = row.find('td:eq(3)').text();
                // Set values in the modal form
                $('#employee_id').val(employee_id);
                $('#name').val(name);
                $('#email').val(email);
                $('#userName').val(userName);
                // Show the modal
                $('#addEmployeeModal').modal('show');
            });
        });
    </script>

    <script>
        function selectSureName(selectedName,selectedId) {
            var surenameOptions = document.getElementsByClassName('surename-option');

            for (var i = 0; i < surenameOptions.length; i++) {
                surenameOptions[i].style.backgroundColor = 'lightgray';
                surenameOptions[i].style.color = 'black';
            }
            document.getElementById(selectedName).style.backgroundColor = 'slateblue';
            document.getElementById(selectedName).style.color = 'white';
            document.getElementById('selectSureName1').value = selectedId;
        }
        function selectEditSureName(selectedName,selectedId) {
            console.log('edit'+selectedId);
            var surenameOptions = document.getElementsByClassName('editsurename-option');

            for (var i = 0; i < surenameOptions.length; i++) {
                surenameOptions[i].style.backgroundColor = 'lightgray';
                surenameOptions[i].style.color = 'black';
            }
            document.getElementById('edit'+selectedId).style.backgroundColor = 'slateblue';
            document.getElementById('edit'+selectedId).style.color = 'white';
            document.getElementById('editSureName1').value = selectedId;
        }
        function addPlace() {
            var input = document.getElementById('myInput');
            input.placeholder = '+966---------';
        }
        function editaddPlace() {
            var input = document.getElementById('editmyInput');
            input.placeholder = '+966---------';
        }
        function formatPhoneNumber(input) {
            var phoneNumber = input.value;
            if (!phoneNumber.startsWith('+966')) {
                input.value = '+966';
            }
            if (phoneNumber.length > 13) {
                input.value = phoneNumber.slice(0, 13);
            }

        }
        function editformatPhoneNumber(input) {
            var phoneNumber = input.value;
            if (!phoneNumber.startsWith('+966')) {
                input.value = '+966';
            }
            if (phoneNumber.length > 13) {
                input.value = phoneNumber.slice(0, 13);
            }

        }
        function selectLanguage(language) {
            if (language === 'arabic') {
                document.getElementById('arabicSelector').style.backgroundColor = 'slateblue';
                document.getElementById('arabicSelector').style.color = 'white';
                document.getElementById('englishSelector').style.backgroundColor = 'lightgray';
                document.getElementById('englishSelector').style.color = 'black';
                document.getElementById('selectedLanguage').value = 'عربي';
            } else if (language === 'english') {
                document.getElementById('englishSelector').style.backgroundColor = 'slateblue';
                document.getElementById('englishSelector').style.color = 'white';
                document.getElementById('arabicSelector').style.backgroundColor = 'lightgray';
                document.getElementById('arabicSelector').style.color = 'black';
                document.getElementById('selectedLanguage').value = 'انكليزي';
            }
        }
        function editselectLanguage(language) {
            if (language === 'arabic') {
                document.getElementById('editarabicSelector').style.backgroundColor = 'slateblue';
                document.getElementById('editarabicSelector').style.color = 'white';
                document.getElementById('editenglishSelector').style.backgroundColor = 'lightgray';
                document.getElementById('editenglishSelector').style.color = 'black';
                document.getElementById('editselectedLanguage').value = 'عربي';
            } else if (language === 'english') {
                document.getElementById('editenglishSelector').style.backgroundColor = 'slateblue';
                document.getElementById('editenglishSelector').style.color = 'white';
                document.getElementById('editarabicSelector').style.backgroundColor = 'lightgray';
                document.getElementById('editarabicSelector').style.color = 'black';
                document.getElementById('editselectedLanguage').value = 'انكليزي';
            }
        }
        function selectSendMessage(val) {
            if (val === 'true') {
                document.getElementById('trueSelectMessage').style.backgroundColor = 'green';
                document.getElementById('trueSelectMessage').style.color = 'white';
                document.getElementById('falseSelectMessage').style.backgroundColor = 'lightgray';
                document.getElementById('falseSelectMessage').style.color = 'black';
                document.getElementById('send_message').value = 'true';
            } else if (val === 'false') {
                document.getElementById('falseSelectMessage').style.backgroundColor = 'slateblue';
                document.getElementById('falseSelectMessage').style.color = 'white';
                document.getElementById('trueSelectMessage').style.backgroundColor = 'lightgray';
                document.getElementById('trueSelectMessage').style.color = 'black';
                document.getElementById('send_message').value = 'false';
            }
        }
        function editselectSendMessage(val) {
            if (val === 'true') {
                document.getElementById('edittrueSelectMessage').style.backgroundColor = 'green';
                document.getElementById('edittrueSelectMessage').style.color = 'white';
                document.getElementById('editfalseSelectMessage').style.backgroundColor = 'lightgray';
                document.getElementById('editfalseSelectMessage').style.color = 'black';
                document.getElementById('editsend_message').value = 'true';
            } else if (val === 'false') {
                document.getElementById('editfalseSelectMessage').style.backgroundColor = 'slateblue';
                document.getElementById('editfalseSelectMessage').style.color = 'white';
                document.getElementById('edittrueSelectMessage').style.backgroundColor = 'lightgray';
                document.getElementById('edittrueSelectMessage').style.color = 'black';
                document.getElementById('editsend_message').value = 'false';
            }
        }
        function selectSendEmail(val) {
            if (val === 'true') {
                document.getElementById('trueSelectEmail').style.backgroundColor = 'green';
                document.getElementById('trueSelectEmail').style.color = 'white';
                document.getElementById('falseSelectEmail').style.backgroundColor = 'lightgray';
                document.getElementById('falseSelectEmail').style.color = 'black';
                document.getElementById('send_email').value = 'true';
            } else if (val === 'false') {
                document.getElementById('falseSelectEmail').style.backgroundColor = 'slateblue';
                document.getElementById('falseSelectEmail').style.color = 'white';
                document.getElementById('trueSelectEmail').style.backgroundColor = 'lightgray';
                document.getElementById('trueSelectEmail').style.color = 'black';
                document.getElementById('send_email').value = 'false';
            }
        }
        function editselectSendEmail(val) {
            if (val === 'true') {
                document.getElementById('edittrueSelectEmail').style.backgroundColor = 'green';
                document.getElementById('edittrueSelectEmail').style.color = 'white';
                document.getElementById('editfalseSelectEmail').style.backgroundColor = 'lightgray';
                document.getElementById('editfalseSelectEmail').style.color = 'black';
                document.getElementById('editsend_email').value = 'true';
            } else if (val === 'false') {
                document.getElementById('editfalseSelectEmail').style.backgroundColor = 'slateblue';
                document.getElementById('editfalseSelectEmail').style.color = 'white';
                document.getElementById('edittrueSelectEmail').style.backgroundColor = 'lightgray';
                document.getElementById('edittrueSelectEmail').style.color = 'black';
                document.getElementById('editsend_email').value = 'false';
            }
        }
        function selectConfirm(val) {
            if (val === 'true') {
                document.getElementById('trueConfirm').style.backgroundColor = 'slateblue';
                document.getElementById('trueConfirm').style.color = 'white';
                document.getElementById('falseConfirm').style.backgroundColor = 'lightgray';
                document.getElementById('falseConfirm').style.color = 'black';
                document.getElementById('confirm').value = 'true';
            } else if (val === 'false') {
                document.getElementById('falseConfirm').style.backgroundColor = 'red';
                document.getElementById('falseConfirm').style.color = 'white';
                document.getElementById('trueConfirm').style.backgroundColor = 'lightgray';
                document.getElementById('trueConfirm').style.color = 'black';
                document.getElementById('confirm').value = 'false';
            }
        }
        function editselectConfirm(val) {
            if (val === 'true') {
                document.getElementById('edittrueConfirm').style.backgroundColor = 'slateblue';
                document.getElementById('edittrueConfirm').style.color = 'white';
                document.getElementById('editfalseConfirm').style.backgroundColor = 'lightgray';
                document.getElementById('editfalseConfirm').style.color = 'black';
                document.getElementById('editconfirm').value = 'true';
            } else if (val === 'false') {
                document.getElementById('editfalseConfirm').style.backgroundColor = 'red';
                document.getElementById('editfalseConfirm').style.color = 'white';
                document.getElementById('edittrueConfirm').style.backgroundColor = 'lightgray';
                document.getElementById('edittrueConfirm').style.color = 'black';
                document.getElementById('editconfirm').value = 'false';
            }
        }
    </script>
    <script>
        $(document).ready(function(){
            $('.details-btn').click(function(){
                var userName = $(this).data('name');
                var userEmail = $(this).data('email');
                var userNumber = $(this).data('number');
                var userPosition =  $(this).data('position');
                var userDestination = $(this).data('destination');
                $('#modalUserName').text(userName);
                $('#modalUserEmail').text(userEmail);
                $('#modalUserPosition').text(userPosition);
                $('#modalUserDestination').text(userDestination);
                $('#modalUserNumber').text(userNumber);
                $('#userDetailsModal').modal('show');
            });
        });
    </script>

    <script>
        $(document).ready(function(){
            $('.seat-btn').click(function(){

                var id = $(this).data('id');
                $('#seatInvId').val(id);
                $('#seatModal').modal('show');
            });
        });
    </script>
    <script>
        $(document).ready(function(){
            $('.edit-btn').click(function(){
                var userName = $(this).data('name');
                var userEmail = $(this).data('email');
                var userNumber = $(this).data('number');
                var userPosition = $(this).data('position');
                var userDestination = $(this).data('destination');
                var surename = $(this).data('surename');
                var surename2 = $(this).data('surename2');
                var group = $(this).data('group');
                var confirm = $(this).data('confirm');
                var id = $(this).data('id');
                $('#editId').val(id);
                $('#editSureName1').val(surename);
                $('#editSureName2').val(surename2);
                $('#editgroup').val(group);
                $('#editemail').val(userEmail);
                $('#editname').val(userName);
                $('#editdestination').val(userDestination);
                $('#editposition').val(userPosition);
                $('#editmyInput').val(userNumber);
                $('#editconfirm').val(confirm);
                document.getElementById('edit'+surename).style.backgroundColor = 'slateblue';
                document.getElementById('edit'+surename).style.color = 'white';
                if(confirm)
                {
                    document.getElementById('edittrueConfirm').style.backgroundColor = 'slateblue';
                    document.getElementById('edittrueConfirm').style.color = 'white';
                    document.getElementById('editfalseConfirm').style.backgroundColor = 'lightgray';
                    document.getElementById('editfalseConfirm').style.color = 'black';
                }
                else if(!confirm)
                {
                    document.getElementById('editfalseConfirm').style.backgroundColor = 'red';
                    document.getElementById('editfalseConfirm').style.color = 'white';
                    document.getElementById('edittrueConfirm').style.backgroundColor = 'lightgray';
                    document.getElementById('edittrueConfirm').style.color = 'black';
                }

                $('#editInvitation').modal('show');
            });
        });

    </script>


@endsection
