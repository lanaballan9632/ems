<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@300;400;600&display=swap" rel="stylesheet">
    <title>Invitation</title>
    <style>
        .ar-font{
            font-family: 'Cairo', sans-serif;
        }
        .sub-a
        {
            text-decoration: none;
            background-color: slateblue;
            color: white;
            border-radius: 20px;
            padding: 10px 15px 10px 15px;
            width: 50px;
            font-size: 18px;
        }
    </style>
</head>
<body class="ar-font">
<h2 style="text-align: center; font-weight: bold;">
    {{ $mailData['surename'] }}: {{ $mailData['name'] }}
</h2>
<h3 style="text-align: center">تم استلام طلبكم بنجاح, وسيتم التواصل معكم لتأكيد حالة التسجيل قريبا"</h3>


</body>
</html>
