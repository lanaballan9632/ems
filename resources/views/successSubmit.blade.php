<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>تسجيل طلب</title>
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@300;400;600&display=swap" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <!-- Include jQuery -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <!-- Include Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <style>
        .ar-font{
            font-family: 'Cairo', sans-serif;
        }
        .slate-blue-shadow {
            box-shadow: 0 0 5px slateblue;
        }
        .submit-btn,.submit-btn:hover{
            font-family: 'Cairo', sans-serif;
            background-color: #4F42B5;
            border-color: #4F42B5;
            border-radius: 20px;
            font-size: 18px;
            width: 90px;
        }
    </style>
</head>
<body class="antialiased bg-light">

<nav class="navbar navbar-expand-sm bg-white navbar-light pt-4 pb-4 pl-5 mt-0">
    <div class="container">
        <!-- Your navigation content -->
    </div>
</nav>

<div class="container mt-4 ar-font text-end">
    <div class="p-4 mb-5" style="background-color: forestgreen;color: white;">
        <span>تم التأكيد بنجاح. شكرا" لكم </span>
    </div>

</div>

</body>
</html>
