<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>تأكيد الحضور</title>
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@300;400;600&display=swap" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <!-- Include jQuery -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <!-- Include Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <style>
        .ar-font{
            font-family: 'Cairo', sans-serif;
        }
        .slate-blue-shadow {
            box-shadow: 0 0 5px slateblue;
        }
        .submit-btn,.submit-btn:hover{
            font-family: 'Cairo', sans-serif;
            background-color: #4F42B5;
            border-color: #4F42B5;
            border-radius: 0px;
            font-size: 18px;
            width: 90px;
        }
    </style>
</head>
<body class="antialiased bg-light">

<nav class="navbar navbar-expand-sm bg-white navbar-light pt-4 pb-4 pl-5 mt-0">
    <div class="container">
        <!-- Your navigation content -->
    </div>
</nav>

<div class="container mt-4 ar-font">
    @if(Session::get('fail'))
        <div style="background-color: red; color: white;text-align: right; padding: 10px"
             class="alert alert-danger">{{Session::get('fail')}}</div>
    @endif
<div class="text-center p-2 mb-5" style="background-color: slateblue;color: white; border-radius: 10px">
    <h4>البيانات </h4>
</div>
        <form class="text-end" method="POST" role="form" action="/submit/confirm/{{$invitation->id}}">
            @csrf
            <div class="row mb-3">
                <div class="col-6">
                    <label for="name" class="form-label">الاسم الكامل</label>
                    <input readonly type="text" class="form-control text-end slate-blue-shadow text-end" id="name" name="name" value="{{$invitation->name}}" required>

                </div>
                <div class="col-6">
                    <label for="email" class="form-label">البريد الالكتروني</label>
                    <input readonly type="email" class="form-control text-end slate-blue-shadow text-end " id="email" name="email" value="{{$invitation->email}}" required >
                </div>


            </div>
            <div class="row mb-3">
                <div class="col-6">
                    <label for="destination" class="form-label">الجهة</label>
                    <input readonly type="text" class="form-control text-end slate-blue-shadow"  id="destination" name="destination" value="{{$invitation->destination}}" required>

                </div>
                <div class="col-6">
                    <label for="number" class="form-label">رقم الجوال</label>
                    <input readonly type="text" name="number" class="form-control slate-blue-shadow text-end " id="number" value="{{$invitation->number}}" maxlength="13" required>
                </div>

            </div>
            <div class="row mb-5">
                <div class="col-6">
                </div>
                <div class="col-6">
                    <label for="position" class="form-label">المنصب</label>
                    <input readonly type="text" class="form-control text-end slate-blue-shadow" value="{{$invitation->position}}" id="position" name="position" required>
                </div>

            </div>
            <button type="submit" class="btn btn-primary submit-btn" id="submitForm">التأكيد</button>
        </form>
</div>

</body>
</html>
