<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::middleware(['guest:web'])->group(function () {
    Route::view('/login', 'login2')->name('login');
    Route::post('/check', [\App\Http\Controllers\Admin\AuthController::class, 'check'])->name('check');
});
Route::get('/', [\App\Http\Controllers\Admin\GeneralInvitationController::class, 'addReq']);
Route::post('/store/invitation/req', [\App\Http\Controllers\Admin\GeneralInvitationController::class, 'storeReq']);
Route::get('/confirm/mail/{id}', [\App\Http\Controllers\Admin\InvitationController::class, 'confirmMail']);
Route::post('/submit/confirm/{id}', [\App\Http\Controllers\Admin\InvitationController::class, 'submitInvitation']);

Route::middleware(['auth:web'])->group(function () {
    Route::get('/home', [\App\Http\Controllers\Admin\AuthController::class, 'home'])->name('home');
    Route::get('/logout', [\App\Http\Controllers\Admin\AuthController::class, 'adminLogout'])->name('logout');
    Route::get('/employee', [\App\Http\Controllers\Admin\EmployeeController::class, 'add'])->name('employee');
    Route::post('/store/employee', [\App\Http\Controllers\Admin\EmployeeController::class, 'store']);
    Route::post('/update/employee', [\App\Http\Controllers\Admin\EmployeeController::class, 'update']);
    Route::get('/employee/delete/{id}', [\App\Http\Controllers\Admin\EmployeeController::class, 'delete']);
    Route::post('/update/permission/{id}', [\App\Http\Controllers\Admin\EmployeeController::class, 'updatePermission']);
    Route::get('/surename1', [\App\Http\Controllers\Admin\SurenameController::class, 'add1'])->name('surename1');
    Route::get('/surename2', [\App\Http\Controllers\Admin\SurenameController::class, 'add2'])->name('surename2');
    Route::post('/store/surename1', [\App\Http\Controllers\Admin\SurenameController::class, 'store1']);
    Route::post('/store/surename2', [\App\Http\Controllers\Admin\SurenameController::class, 'store2']);
    Route::get('/surename1/delete/{id}', [\App\Http\Controllers\Admin\SurenameController::class, 'delete1']);
    Route::get('/surename2/delete/{id}', [\App\Http\Controllers\Admin\SurenameController::class, 'delete2']);
    Route::post('/update/surename1', [\App\Http\Controllers\Admin\SurenameController::class, 'update1']);
    Route::post('/update/surename2', [\App\Http\Controllers\Admin\SurenameController::class, 'update2']);
    Route::get('/group', [\App\Http\Controllers\Admin\GroupController::class, 'add'])->name('group');
    Route::post('/store/group', [\App\Http\Controllers\Admin\GroupController::class, 'store']);
    Route::get('/group/delete/{id}', [\App\Http\Controllers\Admin\GroupController::class, 'delete']);
    Route::post('/update/group', [\App\Http\Controllers\Admin\GroupController::class, 'update']);
    Route::get('/send/invitation', [\App\Http\Controllers\Admin\InvitationController::class, 'add'])->name('invitation');
    Route::post('/store/invitation', [\App\Http\Controllers\Admin\InvitationController::class, 'store']);
    Route::get('/send/invitation/delete/{id}', [\App\Http\Controllers\Admin\InvitationController::class, 'delete']);
    Route::post('/update/invitation', [\App\Http\Controllers\Admin\InvitationController::class, 'update']);
    Route::get('/general/invitation', [\App\Http\Controllers\Admin\GeneralInvitationController::class, 'add'])->name('generalinvitation');
    Route::post('/store/general/invitation', [\App\Http\Controllers\Admin\GeneralInvitationController::class, 'store']);
    Route::get('/general/invitation/delete/{id}', [\App\Http\Controllers\Admin\GeneralInvitationController::class, 'delete']);
    Route::post('/update/general/invitation', [\App\Http\Controllers\Admin\GeneralInvitationController::class, 'update']);
    Route::get('/all/invitations/', [\App\Http\Controllers\Admin\InvitationController::class, 'all']);
    Route::post('/store/confirmed/invitation', [\App\Http\Controllers\Admin\InvitationController::class, 'storeConfirmedInvitation']);
    Route::post('/update/confirmed/invitation', [\App\Http\Controllers\Admin\InvitationController::class, 'updateConfirmedInvitation']);
    Route::get('/delete/confirmed/invitation/{id}', [\App\Http\Controllers\Admin\InvitationController::class, 'deleteConfirmedInvitation']);
    Route::post('/set/seat', [\App\Http\Controllers\Admin\InvitationController::class, 'setSeat']);
    Route::get('/all/seats/', [\App\Http\Controllers\Admin\InvitationController::class, 'allSeats']);
    Route::get('/empty/seats', [\App\Http\Controllers\Admin\InvitationController::class, 'emptySeats']);
    Route::post('/update/seat', [\App\Http\Controllers\Admin\InvitationController::class, 'updateSeat']);


});
