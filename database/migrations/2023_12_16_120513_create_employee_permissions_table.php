<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('employee_permissions', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->bigInteger('permission_id');
            $table->timestamps();
        });
        $defaultPermissions = [
            ['user_id' => '1', 'permission_id' => '1'],
            [ 'user_id' => '1', 'permission_id' => '2'],
            [ 'user_id' => '1', 'permission_id' => '3'],
            [ 'user_id' => '1', 'permission_id' => '4'],
            [ 'user_id' => '1', 'permission_id' => '5'],
            [ 'user_id' => '1', 'permission_id' => '6'],
            [ 'user_id' => '1', 'permission_id' => '7'],
            [ 'user_id' => '1', 'permission_id' => '8'],
        ];

        DB::table('employee_permissions')->insert($defaultPermissions);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('employee_permissions');
    }
};
