<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('general_invitations', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('surename')->nullable();
            $table->bigInteger('surename2');
            $table->bigInteger('group')->nullable();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('extra_email')->nullable();
            $table->string('destination');
            $table->string('number');
            $table->string('position');
            $table->boolean('send_email')->default(true);
            $table->string('req_status')->default('قيد الدراسة');
            $table->boolean('attend')->default(false);
            //خارجي أو داخلي
            $table->string('register_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('general_invitations');
    }
};
