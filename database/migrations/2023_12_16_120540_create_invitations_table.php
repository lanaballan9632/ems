<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('invitations', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('surename')->nullable();
            $table->bigInteger('surename2');
            $table->bigInteger('original_id')->nullable();
            $table->bigInteger('group')->nullable();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('invitation_type');
            $table->string('seat_id')->nullable();
            $table->string('destination');
            $table->string('number');
            $table->string('position');
            $table->boolean('attend')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('invitations');
    }
};
