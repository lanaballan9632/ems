<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('seats', function (Blueprint $table) {
            $table->id();
            $table->string('person_name')->nullable();
            $table->String('seat_id');
            $table->String('type');
            $table->String('status')->default('فارغ');

            $table->timestamps();
        });
        $defaultSeats = [
            ['seat_id' => 'A1', 'type' => 'VIP'],
            [ 'seat_id' => 'A2', 'type' => 'VIP'],
            [ 'seat_id' => 'A3', 'type' => 'VIP'],
            [ 'seat_id' => 'A4', 'type' => 'VIP'],
            [ 'seat_id' => 'A5', 'type' => 'VIP'],
            [ 'seat_id' => 'A6', 'type' => 'VIP'],
            [ 'seat_id' => 'A7', 'type' => 'VIP'],
            [ 'seat_id' => 'A8', 'type' => 'VIP'],
            [ 'seat_id' => 'A9', 'type' => 'VIP'],
            [ 'seat_id' => 'A10', 'type' => 'VIP'],
            [ 'seat_id' => 'A11', 'type' => 'VIP'],
            [ 'seat_id' => 'A12', 'type' => 'VIP'],
            [ 'seat_id' => 'A13', 'type' => 'VIP'],
            [ 'seat_id' => 'A14', 'type' => 'VIP'],
            [ 'seat_id' => 'A15', 'type' => 'VIP'],
            [ 'seat_id' => 'A16', 'type' => 'VIP'],
            [ 'seat_id' => 'A17', 'type' => 'VIP'],
            [ 'seat_id' => 'A18', 'type' => 'VIP'],

            [ 'seat_id' => 'B19', 'type' => 'VIP'],
            [ 'seat_id' => 'B20', 'type' => 'VIP'],
            [ 'seat_id' => 'B21', 'type' => 'VIP'],
            [ 'seat_id' => 'B22', 'type' => 'VIP'],
            [ 'seat_id' => 'B23', 'type' => 'VIP'],
            [ 'seat_id' => 'B24', 'type' => 'VIP'],
            [ 'seat_id' => 'B25', 'type' => 'VIP'],
            [ 'seat_id' => 'B26', 'type' => 'VIP'],
            [ 'seat_id' => 'B27', 'type' => 'VIP'],
            [ 'seat_id' => 'B28', 'type' => 'VIP'],
            [ 'seat_id' => 'B29', 'type' => 'VIP'],
            [ 'seat_id' => 'B30', 'type' => 'VIP'],
            [ 'seat_id' => 'B31', 'type' => 'VIP'],
            [ 'seat_id' => 'B32', 'type' => 'VIP'],
            [ 'seat_id' => 'B33', 'type' => 'VIP'],
            [ 'seat_id' => 'B34', 'type' => 'VIP'],

            [ 'seat_id' => 'C35', 'type' => 'VIP'],
            [ 'seat_id' => 'C36', 'type' => 'VIP'],
            [ 'seat_id' => 'C37', 'type' => 'VIP'],
            [ 'seat_id' => 'C38', 'type' => 'VIP'],
            [ 'seat_id' => 'C39', 'type' => 'VIP'],
            [ 'seat_id' => 'C40', 'type' => 'VIP'],
            [ 'seat_id' => 'C41', 'type' => 'VIP'],
            [ 'seat_id' => 'C42', 'type' => 'VIP'],
            [ 'seat_id' => 'C43', 'type' => 'VIP'],
            [ 'seat_id' => 'C44', 'type' => 'VIP'],
            [ 'seat_id' => 'C45', 'type' => 'VIP'],
            [ 'seat_id' => 'C46', 'type' => 'VIP'],
            [ 'seat_id' => 'C47', 'type' => 'VIP'],
            [ 'seat_id' => 'C48', 'type' => 'VIP'],

            [ 'seat_id' => 'D49', 'type' => 'عادي'],
            [ 'seat_id' => 'D50', 'type' => 'عادي'],
            [ 'seat_id' => 'D51', 'type' => 'عادي'],
            [ 'seat_id' => 'D52', 'type' => 'عادي'],
            [ 'seat_id' => 'D53', 'type' => 'عادي'],
            [ 'seat_id' => 'D54', 'type' => 'عادي'],
            [ 'seat_id' => 'D55', 'type' => 'عادي'],
            [ 'seat_id' => 'D56', 'type' => 'عادي'],
            [ 'seat_id' => 'D57', 'type' => 'عادي'],
            [ 'seat_id' => 'D58', 'type' => 'عادي'],
            [ 'seat_id' => 'D59', 'type' => 'عادي'],
            [ 'seat_id' => 'D60', 'type' => 'عادي'],
            [ 'seat_id' => 'D61', 'type' => 'عادي'],
            [ 'seat_id' => 'D62', 'type' => 'عادي'],
            [ 'seat_id' => 'D63', 'type' => 'عادي'],
            [ 'seat_id' => 'D64', 'type' => 'عادي'],

            [ 'seat_id' => 'E65', 'type' => 'عادي'],
            [ 'seat_id' => 'E66', 'type' => 'عادي'],
            [ 'seat_id' => 'E67', 'type' => 'عادي'],
            [ 'seat_id' => 'E68', 'type' => 'عادي'],
            [ 'seat_id' => 'E69', 'type' => 'عادي'],
            [ 'seat_id' => 'E70', 'type' => 'عادي'],
            [ 'seat_id' => 'E71', 'type' => 'عادي'],
            [ 'seat_id' => 'E72', 'type' => 'عادي'],
            [ 'seat_id' => 'E73', 'type' => 'عادي'],
            [ 'seat_id' => 'E74', 'type' => 'عادي'],
            [ 'seat_id' => 'E75', 'type' => 'عادي'],
            [ 'seat_id' => 'E76', 'type' => 'عادي'],
            [ 'seat_id' => 'E77', 'type' => 'عادي'],
            [ 'seat_id' => 'E78', 'type' => 'عادي'],
            [ 'seat_id' => 'E79', 'type' => 'عادي'],
            [ 'seat_id' => 'E80', 'type' => 'عادي'],

            [ 'seat_id' => 'F81', 'type' => 'عادي'],
            [ 'seat_id' => 'F82', 'type' => 'عادي'],
            [ 'seat_id' => 'F83', 'type' => 'عادي'],
            [ 'seat_id' => 'F84', 'type' => 'عادي'],
            [ 'seat_id' => 'F85', 'type' => 'عادي'],
            [ 'seat_id' => 'F86', 'type' => 'عادي'],
            [ 'seat_id' => 'F87', 'type' => 'عادي'],
            [ 'seat_id' => 'F88', 'type' => 'عادي'],
            [ 'seat_id' => 'F89', 'type' => 'عادي'],
            [ 'seat_id' => 'F90', 'type' => 'عادي'],
            [ 'seat_id' => 'F91', 'type' => 'عادي'],
            [ 'seat_id' => 'F92', 'type' => 'عادي'],
            [ 'seat_id' => 'F93', 'type' => 'عادي'],
            [ 'seat_id' => 'F94', 'type' => 'عادي'],
            [ 'seat_id' => 'F95', 'type' => 'عادي'],
            [ 'seat_id' => 'F96', 'type' => 'عادي'],
            [ 'seat_id' => 'F97', 'type' => 'عادي'],
            [ 'seat_id' => 'F98', 'type' => 'عادي'],

            [ 'seat_id' => 'G99', 'type' => 'عادي'],
            [ 'seat_id' => 'G100', 'type' => 'عادي'],
            [ 'seat_id' => 'G101', 'type' => 'عادي'],
            [ 'seat_id' => 'G102', 'type' => 'عادي'],
            [ 'seat_id' => 'G103', 'type' => 'عادي'],
            [ 'seat_id' => 'G104', 'type' => 'عادي'],
            [ 'seat_id' => 'G105', 'type' => 'عادي'],
            [ 'seat_id' => 'G106', 'type' => 'عادي'],
            [ 'seat_id' => 'G107', 'type' => 'عادي'],
            [ 'seat_id' => 'G108', 'type' => 'عادي'],
            [ 'seat_id' => 'G109', 'type' => 'عادي'],
            [ 'seat_id' => 'G110', 'type' => 'عادي'],
            [ 'seat_id' => 'G111', 'type' => 'عادي'],
            [ 'seat_id' => 'G112', 'type' => 'عادي'],
            [ 'seat_id' => 'G113', 'type' => 'عادي'],
            [ 'seat_id' => 'G114', 'type' => 'عادي'],
            [ 'seat_id' => 'G115', 'type' => 'عادي'],
            [ 'seat_id' => 'G116', 'type' => 'عادي'],

            [ 'seat_id' => 'H117', 'type' => 'عادي'],
            [ 'seat_id' => 'H118', 'type' => 'عادي'],
            [ 'seat_id' => 'H119', 'type' => 'عادي'],
            [ 'seat_id' => 'H120', 'type' => 'عادي'],
            [ 'seat_id' => 'H121', 'type' => 'عادي'],
            [ 'seat_id' => 'H122', 'type' => 'عادي'],
            [ 'seat_id' => 'H123', 'type' => 'عادي'],
            [ 'seat_id' => 'H124', 'type' => 'عادي'],
            [ 'seat_id' => 'H125', 'type' => 'عادي'],
            [ 'seat_id' => 'H126', 'type' => 'عادي'],
            [ 'seat_id' => 'H127', 'type' => 'عادي'],
            [ 'seat_id' => 'H128', 'type' => 'عادي'],
            [ 'seat_id' => 'H129', 'type' => 'عادي'],
            [ 'seat_id' => 'H130', 'type' => 'عادي'],
            [ 'seat_id' => 'H131', 'type' => 'عادي'],
            [ 'seat_id' => 'H132', 'type' => 'عادي'],
            [ 'seat_id' => 'H133', 'type' => 'عادي'],
            [ 'seat_id' => 'H134', 'type' => 'عادي'],

            [ 'seat_id' => 'I135', 'type' => 'عادي'],
            [ 'seat_id' => 'I136', 'type' => 'عادي'],
            [ 'seat_id' => 'I137', 'type' => 'عادي'],
            [ 'seat_id' => 'I138', 'type' => 'عادي'],
            [ 'seat_id' => 'I139', 'type' => 'عادي'],
            [ 'seat_id' => 'I140', 'type' => 'عادي'],
            [ 'seat_id' => 'I141', 'type' => 'عادي'],
            [ 'seat_id' => 'I142', 'type' => 'عادي'],
            [ 'seat_id' => 'I143', 'type' => 'عادي'],
            [ 'seat_id' => 'I144', 'type' => 'عادي'],
            [ 'seat_id' => 'I145', 'type' => 'عادي'],
            [ 'seat_id' => 'I146', 'type' => 'عادي'],
            [ 'seat_id' => 'I147', 'type' => 'عادي'],
            [ 'seat_id' => 'I148', 'type' => 'عادي'],
            [ 'seat_id' => 'I149', 'type' => 'عادي'],
            [ 'seat_id' => 'I150', 'type' => 'عادي'],
            [ 'seat_id' => 'I151', 'type' => 'عادي'],
            [ 'seat_id' => 'I152', 'type' => 'عادي'],

            [ 'seat_id' => 'J153', 'type' => 'عادي'],
            [ 'seat_id' => 'J154', 'type' => 'عادي'],
            [ 'seat_id' => 'J155', 'type' => 'عادي'],
            [ 'seat_id' => 'J156', 'type' => 'عادي'],
            [ 'seat_id' => 'J157', 'type' => 'عادي'],
            [ 'seat_id' => 'J158', 'type' => 'عادي'],
            [ 'seat_id' => 'J159', 'type' => 'عادي'],
            [ 'seat_id' => 'J160', 'type' => 'عادي'],
            [ 'seat_id' => 'J161', 'type' => 'عادي'],
            [ 'seat_id' => 'J162', 'type' => 'عادي'],
            [ 'seat_id' => 'J163', 'type' => 'عادي'],
            [ 'seat_id' => 'J164', 'type' => 'عادي'],
            [ 'seat_id' => 'J165', 'type' => 'عادي'],
            [ 'seat_id' => 'J166', 'type' => 'عادي'],
            [ 'seat_id' => 'J167', 'type' => 'عادي'],
            [ 'seat_id' => 'J168', 'type' => 'عادي'],
            [ 'seat_id' => 'J169', 'type' => 'عادي'],
            [ 'seat_id' => 'J170', 'type' => 'عادي'],

            [ 'seat_id' => 'K171', 'type' => 'عادي'],
            [ 'seat_id' => 'K172', 'type' => 'عادي'],
            [ 'seat_id' => 'K173', 'type' => 'عادي'],
            [ 'seat_id' => 'K174', 'type' => 'عادي'],
            [ 'seat_id' => 'K175', 'type' => 'عادي'],
            [ 'seat_id' => 'K176', 'type' => 'عادي'],
            [ 'seat_id' => 'K177', 'type' => 'عادي'],
            [ 'seat_id' => 'K178', 'type' => 'عادي'],
            [ 'seat_id' => 'K179', 'type' => 'عادي'],
            [ 'seat_id' => 'K180', 'type' => 'عادي'],
            [ 'seat_id' => 'K181', 'type' => 'عادي'],
            [ 'seat_id' => 'K182', 'type' => 'عادي'],
            [ 'seat_id' => 'K183', 'type' => 'عادي'],
            [ 'seat_id' => 'K184', 'type' => 'عادي'],
            [ 'seat_id' => 'K185', 'type' => 'عادي'],
            [ 'seat_id' => 'K186', 'type' => 'عادي'],
            [ 'seat_id' => 'K187', 'type' => 'عادي'],
            [ 'seat_id' => 'K188', 'type' => 'عادي'],

        ];

        DB::table('seats')->insert($defaultSeats);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('seats');
    }
};
