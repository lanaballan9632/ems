<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->id();
            $table->string('permission_name')->unique();
            $table->timestamps();
        });
        $roles = [
            'إرسال الدعوات',
            'الدعوات العامة',
            'الدعوات المقبولة يوم الحفل',
            'qrcode',
            'تعيين الكراسي',
            'الاطلاع على الدشبورد',
            'إدارة الموظفين',
            'الاطلاع على سجل التغييرات',
        ];

        foreach ($roles as $role) {
            \App\Models\Permission::create([
                'permission_name' => $role
            ]);
        }
    }
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('permissions');
    }
};
