<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('send_invitations', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('surename');
            $table->bigInteger('surename2');
            $table->bigInteger('group');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('extra_email')->nullable();
            $table->string('destination');
            $table->string('number');
            $table->string('position');
            $table->string('lang')->default('عربي');
            $table->boolean('send_email')->default(true);
            $table->boolean('send_message')->default(false);
            $table->boolean('confirm')->default(false);
            $table->boolean('attend')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('send_invitations');
    }
};
